<?php
require_once('inc/database.php');

if (!isset($_POST['user']) || !isset($_POST['mail']) || !isset($_POST['hash'])) {
	http_response_code(401);
	echo "Problème d'authentification : variables utilisateur non définies";
	exit;
}
$user=$_POST['user'];
$email_address=$_POST['mail'];
$hash=$_POST['hash'];
if (sha1(SECRET_UPLOAD_HASH.$user)!=$hash) {
	http_response_code(401);
	echo "Problème d'authentification : impossible de vérifier le hash";
	exit;
}


if (!isset($_POST['id']) || !isset($_POST['title'])) {
	http_response_code(400);
	echo "Erreur : pas de paramètres";
	exit;
}
$id=$_POST['id'];
$title=$_POST['title'];

$db = new Db;
if (!$db->renamevid($id, $title, $user)) {
	echo "Impossible de changer le titre de la vidéo dans la base de données";
	http_response_code(409);
	exit;
}

?>
