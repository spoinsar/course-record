<?php
// Muaz Khan     - www.MuazKhan.com 
// MIT License   - https://www.webrtc-experiment.com/licence/
// Documentation - https://github.com/muaz-khan/RecordRTC


//header("Access-Control-Allow-Origin: *");
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('inc/config.php');
require_once('inc/database.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require_once('3rdparty/PHPMailer/src/Exception.php');
require_once('3rdparty/PHPMailer/src/PHPMailer.php');
require_once('3rdparty/PHPMailer/src/SMTP.php');

//set_error_handler("errorhandler");


if (!isset($_POST['user']) || !isset($_POST['mail']) || !isset($_POST['hash'])) {
	echo "Problème d'authentification : variables utilisateur non définies";
	exit;
}
$user=$_POST['user'];
$email_address=$_POST['mail'];
$hash=$_POST['hash'];
if (sha1(SECRET_UPLOAD_HASH.$user)!=$hash) {
	echo "Problème d'authentification : impossible de vérifier le hash";
	exit;
}

/*function errorhandler($errno, $errstr) {
    echo '<h2>Upload failed.</h2><br>';
    echo '<p>'.$errstr.'</p>';
}*/

function selfInvoker()
{
	global $user, $email_address;
	if (!isset($_POST['audiofilename']) && !isset($_POST['videofilename'])) {
		echo 'Empty file name.';
		return;
	}

	// do NOT allow empty file names
	if (empty($_POST['audiofilename']) && empty($_POST['videofilename'])) {
		echo 'Empty file name.';
		return;
	}

	// do NOT allow third party audio uploads
	if (false && isset($_POST['audiofilename']) && strrpos($_POST['audiofilename'], "RecordRTC-") !== 0) {
		echo 'File name must start with "RecordRTC-"';
		return;
	}

	// do NOT allow third party video uploads
	if (false && isset($_POST['videofilename']) && strrpos($_POST['videofilename'], "RecordRTC-") !== 0) {
		echo 'File name must start with "RecordRTC-"';
		return;
	}

	$fileName = '';
	$tempName = '';
	$file_idx = '';

	if (!empty($_FILES['audio-blob'])) {
		$file_idx = 'audio-blob';
		$fileName = $_POST['audiofilename'];
		$tempName = $_FILES[$file_idx]['tmp_name'];
	} else {
		$file_idx = 'videoblob';
		$fileName = $_POST['videofilename'];
		$tempName = $_FILES[$file_idx]['tmp_name'];
	}

	if (empty($fileName) || empty($tempName)) {
		if(empty($tempName)) {
			echo 'Invalid temp_name: '.$tempName;
			return;
		}

		echo 'Invalid file name: '.$fileName;
		return;
	}


	$filePath = 'uploads/' . $fileName;

	// make sure that one can upload only allowed audio/video files
	$allowed = array(
		'webm',
		'wav',
		'mp4',
		'm4v',
		'mkv',
		'mp3',
		'ogg',
		'opus',
		'mov',
		'wmv',
		'avi',
		'mpg',
		'mpeg',
		'ogv'
	);
	$extension = pathinfo($filePath, PATHINFO_EXTENSION);
	if (!$extension || empty($extension) || !in_array($extension, $allowed)) {
		echo 'Invalid file extension: '.$extension;
		return;
	}

	if (!move_uploaded_file($tempName, $filePath)) {
		if(!empty($_FILES["file"]["error"])) {
			$listOfErrors = array(
				'1' => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
				'2' => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
				'3' => 'The uploaded file was only partially uploaded.',
				'4' => 'No file was uploaded.',
				'6' => 'Missing a temporary folder. Introduced in PHP 5.0.3.',
				'7' => 'Failed to write file to disk. Introduced in PHP 5.1.0.',
				'8' => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.'
			);
			$error = $_FILES["file"]["error"];

			if(!empty($listOfErrors[$error])) {
				echo $listOfErrors[$error];
			}
			else {
				echo 'Not uploaded because of error #'.$_FILES["file"]["error"];
			}
		}
		else {
			echo 'Problem saving file: '.$tempName;
		}
		return;
	}
	$db=new Db;
	$db->insertvid($fileName, $user);
	$serviceurl="https://".$_SERVER['SERVER_NAME'].substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/'));
	$downloadurl=$serviceurl."/player.php?name=".pathinfo($fileName)['filename'];

	$mail = new PHPMailer(true);
	try {
		//Server settings
//		$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
		$mail->isSMTP();                                            // Send using SMTP
		$mail->Host       = SMTP_HOST;                    // Set the SMTP server to send through
		$mail->Port       = SMTP_PORT;
		$mail->SMTPAuth   = false;                                   // Enable SMTP authentication
		$mail->CharSet    = 'UTF-8';
		$mail->Encoding   = 'base64';
//		$mail->Username   = 'user@example.com';                     // SMTP username
//		$mail->Password   = 'secret';                               // SMTP password
//		$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
//		$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

		//Recipients
		$mail->setFrom(SMTP_SENDER_EMAIL, SMTP_SENDER_NAME);
		$mail->addAddress($email_address);     // Add a recipient
		$mail->addReplyTo(SMTP_SENDER_EMAIL, SMTP_SENDER_NAME);

		// Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'Enregistrement de vidéo pédagogique';
		$mail->Body    = <<<"MAILEND"
<p>Bonjour,<p>
<p>Suite à l'enregistrement de votre vidéo pédagogique, elle est disponible ici :</p>
<p><a style="font-family:monospace;" href="$downloadurl">$downloadurl</a></p>
<p>Merci d'avoir utilisé le service "<a href="$serviceurl">record</a>" proposé par la Cellule d'Appui Pédagogique. Vous pouvez retourner sur le site pour gérer la liste de vos vidéos. N'hésitez pas à nous contacter pour tout retour d'expérience ou problème rencontré lors de l'utilisation du service.</p>
<p>Bien cordialement, l'équipe CAP</p>
MAILEND;
		$mail->AltBody = <<<"RAWMAILEND"
Bonjour,
Suite à l'enregistrement de votre vidéo pédagogique, elle est disponible ici :
$downloadurl

Merci d'avoir utilisé le service "record" ( $serviceurl ) proposé par la Cellule d'Appui Pédagogique. Vous pouvez retourner sur le site pour gérer la liste de vos vidéos. N'hésitez pas à nous conacter pour tout retour d'expérience ou problème rencontré lors de l'utilisation du service.

Bien cordialement, l'équipe CAP
RAWMAILEND;

		$mail->send();
	} catch (Exception $e) {
		echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}



	echo 'success';
}


selfInvoker();
?>
