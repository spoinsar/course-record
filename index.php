<?php

if(!isset($_GET['anonymous'])) {
	require_once('inc/config.php');
	require_once('inc/cas.php');
	require_once('inc/database.php');
    if (!$login) {
		showerror('Problème d\'authentification... Login / mot de passe incorrect ?');
	}
	$email_address=$cas_attributes['mail'];
	$hash=sha1(SECRET_UPLOAD_HASH.$login);
} else {
	$anonymous=true;
}
?>

<!--
> Muaz Khan     - www.MuazKhan.com
> MIT License   - www.WebRTC-Experiment.com/licence
> Documentation - github.com/muaz-khan/RecordRTC
> and           - RecordRTC.org
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Enregistrements de vidéos pédagogiques</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link rel="stylesheet" href="style.css">

    <style>
        label {
            display: inline-block;
            width: 8em;
        }

        h1 span {
            background: yellow;
            border: 2px solid #8e1515;
            padding: 2px 8px;
            margin: 2px 5px;
            border-radius: 7px;
            color: #8e1515;
            display: inline-block;
        }

        .recordrtc button {
            font-size: inherit;
        }

        .recordrtc button, .recordrtc select {
            vertical-align: middle;
            line-height: 1;
            padding: 2px 5px;
            height: auto;
            font-size: inherit;
            margin: 0;
        }

        .recordrtc, .recordrtc .header {
            display: block;
            text-align: center;
            padding-top: 0;
        }

        .recordrtc video, .recordrtc img {
	    max-width: 100%!important;
            vertical-align: top;
        }

        .recordrtc audio {
            vertical-align: bottom;
        }

        .recordrtc option[disabled] {
            display: none;
        }

        .recordrtc select {
            font-size: 17px;
	}
#orlabel {
	display:inline-block;
	vertical-align:5rem;
	height:100%;
	margin:0.5em;
	font-size: 1.5em;
}
#btn-start-recording {
	font-size: 1.8em;
}
#startzone {
	display:inline-block;
	vertical-align:4em;
	height:100%;
	border: 0.25em solid #BBBBBB;

}
#startzone * {
	margin:0.4em;
}
#dropzone {
	position: relative;
	display:inline-block;
/*	top: 50%;
	left: 50%;
	margin-top: -100px;
	margin-left: -250px;*/
	width: 200px;
	height: 200px;
	border: 0.25em dashed #BBBBBB;
	margin: 0.4em;
}
#dropzone .icon {
	background-image: url(./images/upload-icon.svg);
	background-size: contain;
	background-repeat:no-repeat;
	position:absolute;
	top:0px;
	left:0px;
	right:0px;
	bottom:0px;
	z-index: 1;
	opacity:0.08;
}
#dropzone p {
	position:absolute;
	top:0px;
	left:0px;
	right:0px;
	bottom:0px;
	height: 100%;
	text-align: center;
	font-size: 1.2rem;
	color: #000000;
	z-index: 2;
}
#dropzone input {
	position: absolute;
	top:0px;
	left:0px;
	right:0px;
	bottom:0px;
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
	outline: none;
	opacity: 0;
	z-index: 999;
}

#warnupdate {
	border:2px solid #FF0000;
	color: #FF0000;
	padding:0.4em;
	margin:0.4em;
	font-size:1.3em;
	margin-top:1.5em;
}

#warnchrome {
	border:2px solid #FF0000;
	padding:0.4em;
	margin:0.4em;
	margin-top:1.5em;
	display:none;
}
#infosupdate {
	border:2px solid #CCCCCC;
	padding:0.4em;
	margin:0.4em;
	margin-top:1.5em;
}
summary {
	text-decoration: underline;
}
td {
	position:relative;
}
.rowaction * {
	margin-right:0.5em;
	cursor: pointer;
}
.rowaction {
	text-align:right;
	padding-right:0.3em;
	
}
table td.timestamp {
	font-size:0.9rem;
}

    </style>

    <script src="RecordRTC.js"></script>
    
    <!-- web streams API polyfill to support Firefox -->
    <script src="js/polyfill.min.js"></script>

    <!-- ../libs/DBML.js to fix video seeking issues -->
    <script src="js/EBML.js"></script>

    <!-- for Edge/FF/Chrome/Opera/etc. getUserMedia support -->
    <script src="js/adapter-latest.js"></script>
    <script src="js/DetectRTC.js"> </script>

    <!-- video element -->
    <link href="js/getHTMLMediaElement.css" rel="stylesheet">
    <script src="js/getHTMLMediaElement.js"></script>
</head>

<body>
    <article>
	<a id="helpbtn" href="https://ics.utc.fr/tuto/screencast.pdf" target="_blank">&#128712; Guide d'utilisation</a>
        <header style="text-align: center;">
            <h1>
                Enregistrement de vidéos pédagogiques</a>
            </h1>

        </header>

	<!--div class="github-stargazers" style="margin-top: 25px;"></div-->
	<!--section class="infos">
		<div id="infosupdate"><b>Maintenance Aujourd'hui 18 septembre de 0:40 à 2:00</b>
		<details>
		<summary>Une maintenance est en cours, le dépot de vidéo est temporairement désactivé pendant la durée de cette maintenance</summary>
		la maintenance est uniquement une maintenance technique interne et ne devrais pas affecter à court ou moyen terme les fonctionnalités ou les données de record.
		<p><b>Un problème ?</b> vous pouvez contacter la <a href="mailto:cap@utc.fr">Cellule d'Appui Pédagogique</a> pour que l'on puisse améliorer l'outil.</p>
		</details>
		</div>
	</section-->

	<section class="warnings">
		<!--div id="warnupdate"><b>Une mise à jour est prévue cette nuit de 2:30 à 3:00. Le service sera temporairement indisponible</b></div-->
		<div id="warnchrome"><b>Attention : utilisez <a target="_blank" href="https://www.google.com/intl/fr_fr/chrome/">Google Chrome</a> pour résoudre des problèmes d'enregistrement.</b>
		<details>
		<summary>Explications...</summary>
		<p>De nombreux utilisateurs nous ont signalé des problèmes dans certains navigateurs internet (curseur non visible, coupure d'enregistrement...). Les retours utilisateurs indiquent qu'il n'y a pas eu ce type de problème avec <a target="_blank" href="https://www.google.com/intl/fr_fr/chrome/">Google Chrome</a> donc nous déconseillons les enregistrements vidéos à partir d'autres navigateurs. Évitez en particulier les enregistrements longs. (Vous pouvez évidement continuer à utiliser d'autres navigateurs pour toutes les activités non liées à cet outil d'enregistrement).
		</p><p>Pour le dépôt d'une vidéo produite depuis un autre outil ou pour la consultation de vidéos par les étudiants (depuis la dernière mise à jour de record), vous pouvez utiliser d'autres navigateurs modernes de votre choix.
		</p>
		</details>
		</div>
	</section>

        <section class="experiment recordrtc">
	    <div class="header" style="margin: 0;">
		<div id="startzone">
                <select class="recording-media">
                    <option value="record-audio-plus-screen">Microphone+Écran de l'ordi</option>
                    <option value="record-audio-plus-video">Microphone+Webcam</option>
                    <option value="record-audio">Microphone</option>
                    <option value="record-screen">Full Screen</option>
                </select>

                <br>

                <button id="btn-start-recording">Lancer l'enregistrement</button>
		<button id="btn-pause-recording" style="display: none; font-size: 15px;">Pause</button>
		</div>
		<span id="orlabel"> ou </span>
                <div id="dropzone"><input id="externalupload" type="file" onchange="handleExternalupload(this.files)"><div class="icon"></div><p>envoyez un fichier vidéo de votre disque dur (drag &amp; drop)</p><!-- Image by Jony from the Noun Project --></div>


		<hr style="border-top: 0;border-bottom: 1px solid rgb(189, 189, 189);margin: 4px -12px;margin-top: 8px;">

            <div style="text-align: center; display: none;">
                <button id="save-to-disk">Récupérer la vidéo (Télécharger)</button>
<?php if (!$anonymous) echo '<button id="upload-to-php">Envoyer sur le serveur UTC</button>'; ?>
                <button id="open-new-tab">Voir dans un nouvel onglet</button>

            </div>

	    <div style="margin-top: 10px;" id="recording-player"></div>
		<br>
                <details class="advoptions">
                <summary>Options à ne pas modifier</summary>

                <select class="media-container-format">
                    <option>default</option>
                    <option>vp8</option>
                    <option>vp9</option>
                    <option>h264</option>
                    <option>mkv</option>
                    <option>opus</option>
                    <option>ogg</option>
                    <option>pcm</option>
                    <option>gif</option>
                    <option>whammy</option>
                    <option>WebAssembly</option>
                </select>

                <div style="display: inline-block;">
                    <input type="checkbox" id="chk-fixSeeking" checked style="margin:0;width:auto;" title="Fix video seeking issues?">
                    <label for="chk-fixSeeking"style="margin:0;width: auto;cursor: pointer;-webkit-user-select:none;user-select:none;" title="Fix video seeking issues?">Fix Seeking Issues?</label>
                </div>
                <input type="checkbox" id="chk-timeSlice" style="margin:0;width:auto;" title="Use intervals based recording">
                <label for="chk-timeSlice" style="margin:0;width: auto;cursor: pointer;-webkit-user-select:none;user-select:none;" title="Use intervals based recording">Use timeSlice?</label>
                <br>
                <select class="media-resolutions">
                    <option value="default">Default resolutions</option>
                    <option value="1920x1080">1080p</option>
                    <option value="1280x720">720p</option>
                    <option value="640x480">480p</option>
                </select>

                <select class="media-framerates">
                    <option value="default">Default framerates</option>
                    <option value="5">5 fps</option>
                    <option value="15">15 fps</option>
                    <option value="24">24 fps</option>
                    <option value="30">30 fps</option>
                    <option value="60">60 fps</option>
                </select>

                <select class="media-bitrates">
                    <option value="default">Default bitrates</option>
                    <option value="32000000">4 Mbps</option>
                    <option value="24000000">3 Mbps</option>
                    <option value="16000000">2 Mbps</option>
                    <option value="1200000">1.5 Mbps</option>
                    <option value="8000000">1 Mbps</option>
                    <option value="6000000">.75 Mbps</option>
                    <option value="4000000">.5 Mbps</option>
                    <option value="3000000">.4 Mbps</option>
                    <option value="2000000">.25 Mbps</option>
                    <option value="1000000">.1 Mbps</option>
                </select>
	    </div>
            </details>
        </section>

        <script>
            (function() {
                var params = {},
                    r = /([^&=]+)=?([^&]*)/g;

                function d(s) {
                    return decodeURIComponent(s.replace(/\+/g, ' '));
                }

                var match, search = window.location.search;
                while (match = r.exec(search.substring(1))) {
                    params[d(match[1])] = d(match[2]);

                    if(d(match[2]) === 'true' || d(match[2]) === 'false') {
                        params[d(match[1])] = d(match[2]) === 'true' ? true : false;
                    }
                }

                window.params = params;
            })();

            function addStreamStopListener(stream, callback) {
                stream.addEventListener('ended', function() {
                    callback();
                    callback = function() {};
                }, false);
                stream.addEventListener('inactive', function() {
                    callback();
                    callback = function() {};
                }, false);
                stream.getTracks().forEach(function(track) {
                    track.addEventListener('ended', function() {
                        callback();
                        callback = function() {};
                    }, false);
                    track.addEventListener('inactive', function() {
                        callback();
                        callback = function() {};
                    }, false);
                });
            }
        </script>

<script>
function showmodal(html) {
	document.querySelector("#statusco").innerHTML = html;
	document.querySelector("#status").className = "experiment modal";
}
function addstatustext(html) {
	document.querySelector("#statusco").innerHTML += html;
}
var avconstraints = {
	audio: true,
	video:{
		cursor: "always",
		width:{
			max: 1920,
			ideal: 1920
//			ideal: 1280
		},
		height: {
			max: 1080,
			ideal: 1080
//			ideal: 720
		},
		frameRate: {
			max: 60,
			ideal: 30
		}
	}
};
// same but no audio
var vconstraints = {
	video:{
		cursor: "always",
		width:{
			max: 1920,
			ideal: 1920
//			ideal: 1280
		},
		height: {
			max: 1080,
			ideal: 1080
//			ideal: 720
		},
		frameRate: {
			max: 60,
			ideal: 30
		}
	}
};
var video = document.createElement('video');
var mediaElement = getHTMLMediaElement(video, {
	title: 'Enregistrement : inactif',
	buttons: ['full-screen'/*, 'take-snapshot'*/],
	showOnMouseEnter: false,
	width: 640,
	onTakeSnapshot: function() {
		var canvas = document.createElement('canvas');
		canvas.width = mediaElement.clientWidth;
		canvas.height = mediaElement.clientHeight;

		var context = canvas.getContext('2d');
		context.drawImage(recordingPlayer, 0, 0, canvas.width, canvas.height);

		window.open(canvas.toDataURL('image/png'));
	}
});
document.getElementById('recording-player').appendChild(mediaElement);

var div = document.createElement('section');
mediaElement.media.parentNode.appendChild(div);
mediaElement.media.muted = false;
mediaElement.media.controls = true;
mediaElement.media.autoplay = true;
mediaElement.media.playsinline = true;

div.appendChild(mediaElement.media);

var recordingPlayer = mediaElement.media;
var recordingMedia = document.querySelector('.recording-media');
var mediaContainerFormat = document.querySelector('.media-container-format');
var mimeType = 'video/webm';
var fileExtension = 'webm';
var type = 'video';
var recorderType;
var defaultWidth;
var defaultHeight;

var btnStartRecording = document.querySelector('#btn-start-recording');

window.onbeforeunload = function() {
	btnStartRecording.disabled = false;
	recordingMedia.disabled = false;
	mediaContainerFormat.disabled = false;

	chkFixSeeking.parentNode.style.display = 'inline-block';
};

btnStartRecording.onclick = function(event) {
	var button = btnStartRecording;

	if(button.innerHTML === 'Arrêter l\'enregistrement') {
		btnPauseRecording.style.display = 'none';
		button.disabled = true;
		button.disableStateWaiting = true;
		setTimeout(function() {
			button.disabled = false;
			button.disableStateWaiting = false;
		}, 2000);

		button.innerHTML = 'Lancer l\'enregistrement';

		function stopStream() {
			if(button.stream && button.stream.stop) {
				button.stream.stop();
				button.stream = null;
			}

			if(button.stream instanceof Array) {
				button.stream.forEach(function(stream) {
					stream.stop();
				});
				button.stream = null;
			}

			videoBitsPerSecond = null;
			var html = 'Enregistrement : arrêté';
			html += '<br>Size: ' + bytesToSize(button.recordRTC.getBlob().size);
			recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = html;
		}

		if(button.recordRTC) {
			if(button.recordRTC.length) {
				button.recordRTC[0].stopRecording(function(url) {
					if(!button.recordRTC[1]) {
						button.recordingEndedCallback(url);
						stopStream();

						saveToDiskOrOpenNewTab(button.recordRTC[0]);
						return;
					}

					button.recordRTC[1].stopRecording(function(url) {
						button.recordingEndedCallback(url);
						stopStream();
					});
				});
			}
			else {
				button.recordRTC.stopRecording(function(url) {
					if(button.blobs && button.blobs.length) {
						var blob = new File(button.blobs, getFileName(fileExtension), {
						type: mimeType
					});

						button.recordRTC.getBlob = function() {
							return blob;
						};

						url = URL.createObjectURL(blob);
					}

					if(chkFixSeeking.checked === true) {
						// to fix video seeking issues
						getSeekableBlob(button.recordRTC.getBlob(), function(seekableBlob) {
							button.recordRTC.getBlob = function() {
								return seekableBlob;
							};

							url = URL.createObjectURL(seekableBlob);

							button.recordingEndedCallback(url);
							saveToDiskOrOpenNewTab(button.recordRTC);
							stopStream();
						})
							return;
					}

					button.recordingEndedCallback(url);
					saveToDiskOrOpenNewTab(button.recordRTC);
					stopStream();
				});
			}
		}

		return;
	}

	if(!event) return;

	button.disabled = true;

	var commonConfig = {
		onMediaCaptured: function(stream) {
			button.stream = stream;
			if(button.mediaCapturedCallback) {
				button.mediaCapturedCallback();
			}

			button.innerHTML = 'Arrêter l\'enregistrement';
			button.disabled = false;

			chkFixSeeking.parentNode.style.display = 'none';
		},
		onMediaStopped: function() {
			button.innerHTML = 'Lancer l\'enregistrement';

			if(!button.disableStateWaiting) {
					button.disabled = false;
			}

			chkFixSeeking.parentNode.style.display = 'inline-block';
		},
		onMediaCapturingFailed: function(error) {
			console.error('onMediaCapturingFailed:', error);

			if(error.toString().indexOf('no audio or video tracks available') !== -1) {
				alert('Impossible de trouver la source audio ou vidéo.');
			} else if(error.name === 'PermissionDeniedError' || error.name === 'NotAllowedError') {
				alert("Impossible d'obtenir la permission d'utiliser le microphone. Recommencez en activant la permission.");
			} else {
				alert('Erreur : '+ error.name +', texte : '+error.toString());
			}


			commonConfig.onMediaStopped();
		}
	};

	if(mediaContainerFormat.value === 'h264') {
		mimeType = 'video/webm\;codecs=h264';
		fileExtension = 'mp4';

		// video/mp4;codecs=avc1    
		if(isMimeTypeSupported('video/mpeg')) {
			mimeType = 'video/mpeg';
		}
	}

	if(mediaContainerFormat.value === 'mkv' && isMimeTypeSupported('video/x-matroska;codecs=avc1')) {
		mimeType = 'video/x-matroska;codecs=avc1';
		fileExtension = 'mkv';
	}

	if(mediaContainerFormat.value === 'vp8' && isMimeTypeSupported('video/webm\;codecs=vp8')) {
		mimeType = 'video/webm\;codecs=vp8';
		fileExtension = 'webm';
		recorderType = null;
		type = 'video';
	}

	if(mediaContainerFormat.value === 'vp9' && isMimeTypeSupported('video/webm\;codecs=vp9')) {
		mimeType = 'video/webm\;codecs=vp9';
		fileExtension = 'webm';
		recorderType = null;
		type = 'video';
	}

	if(mediaContainerFormat.value === 'pcm') {
		mimeType = 'audio/wav';
		fileExtension = 'wav';
		recorderType = StereoAudioRecorder;
		type = 'audio';
	}

	if(mediaContainerFormat.value === 'opus' || mediaContainerFormat.value === 'ogg') {
		if(isMimeTypeSupported('audio/webm')) {
			mimeType = 'audio/webm';
			fileExtension = 'webm'; // webm
		}

		if(isMimeTypeSupported('audio/ogg')) {
			mimeType = 'audio/ogg; codecs=opus';
			fileExtension = 'ogg'; // ogg
		}

		recorderType = null;
		type = 'audio';
	}

	if(mediaContainerFormat.value === 'whammy') {
		mimeType = 'video/webm';
		fileExtension = 'webm';
		recorderType = WhammyRecorder;
		type = 'video';
	}

	if(mediaContainerFormat.value === 'WebAssembly') {
		mimeType = 'video/webm';
		fileExtension = 'webm';
		recorderType = WebAssemblyRecorder;
		type = 'video';
	}

	if(mediaContainerFormat.value === 'gif') {
		mimeType = 'image/gif';
		fileExtension = 'gif';
		recorderType = GifRecorder;
		type = 'gif';
	}

	if(mediaContainerFormat.value === 'default') {
		mimeType = 'video/webm';
		fileExtension = 'webm';
		recorderType = null;
		type = 'video';
	}

	if(recordingMedia.value === 'record-audio') {
		captureAudio(commonConfig);

		button.mediaCapturedCallback = function() {
			var options = {
			type: type,
				mimeType: mimeType,
				leftChannel: params.leftChannel || false,
				disableLogs: params.disableLogs || false
		};

			if(params.sampleRate) {
				options.sampleRate = parseInt(params.sampleRate);
			}

			if(params.bufferSize) {
				options.bufferSize = parseInt(params.bufferSize);
			}

			if(recorderType) {
				options.recorderType = recorderType;
			}

			if(videoBitsPerSecond) {
				options.videoBitsPerSecond = videoBitsPerSecond;
			}

			if(DetectRTC.browser.name === 'Edge') {
				options.numberOfAudioChannels = 1;
			}

			options.ignoreMutedMedia = false;
			button.recordRTC = RecordRTC(button.stream, options);

			button.recordingEndedCallback = function(url) {
				setVideoURL(url);
			};

			button.recordRTC.startRecording();
			btnPauseRecording.style.display = '';
			recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '<img src="images/progress.gif">';
		};
	}

	if(recordingMedia.value === 'record-audio-plus-video') {
		captureAudioPlusVideo(commonConfig);

		button.mediaCapturedCallback = function() {
			if(typeof MediaRecorder === 'undefined') { // opera or chrome etc.
				button.recordRTC = [];

				if(!params.bufferSize) {
					// it fixes audio issues whilst recording 720p
					params.bufferSize = 16384;
				}

				var options = {
				type: 'audio', // hard-code to set "audio"
					leftChannel: params.leftChannel || false,
					disableLogs: params.disableLogs || false,
					video: recordingPlayer
				};

				if(params.sampleRate) {
					options.sampleRate = parseInt(params.sampleRate);
				}

				if(params.bufferSize) {
					options.bufferSize = parseInt(params.bufferSize);
				}

				if(params.frameInterval) {
					options.frameInterval = parseInt(params.frameInterval);
				}

				if(recorderType) {
					options.recorderType = recorderType;
				}

				if(videoBitsPerSecond) {
					options.videoBitsPerSecond = videoBitsPerSecond;
				}

				options.ignoreMutedMedia = false;
				var audioRecorder = RecordRTC(button.stream, options);

				options.type = type;
				var videoRecorder = RecordRTC(button.stream, options);

				// to sync audio/video playbacks in browser!
				videoRecorder.initRecorder(function() {
					audioRecorder.initRecorder(function() {
						audioRecorder.startRecording();
						videoRecorder.startRecording();
						btnPauseRecording.style.display = '';
					});
				});

				button.recordRTC.push(audioRecorder, videoRecorder);

				button.recordingEndedCallback = function() {
					var audio = new Audio();
					audio.src = audioRecorder.toURL();
					audio.controls = true;
					audio.autoplay = true;

					recordingPlayer.parentNode.appendChild(document.createElement('hr'));
					recordingPlayer.parentNode.appendChild(audio);

					if(audio.paused) audio.play();
				};
				return;
			}

			var options = {
			type: type,
				mimeType: mimeType,
				disableLogs: params.disableLogs || false,
				getNativeBlob: false, // enable it for longer recordings
				video: recordingPlayer
			};

			if(recorderType) {
				options.recorderType = recorderType;

				if(recorderType == WhammyRecorder || recorderType == GifRecorder || recorderType == WebAssemblyRecorder) {
					options.canvas = options.video = {
					width: defaultWidth || 320,
						height: defaultHeight || 240
				};
				}
			}

			if(videoBitsPerSecond) {
				options.videoBitsPerSecond = videoBitsPerSecond;
			}

			if(timeSlice && typeof MediaRecorder !== 'undefined') {
				options.timeSlice = timeSlice;
				button.blobs = [];
				options.ondataavailable = function(blob) {
					button.blobs.push(blob);
				};
			}

			options.ignoreMutedMedia = false;
			button.recordRTC = RecordRTC(button.stream, options);

			button.recordingEndedCallback = function(url) {
				setVideoURL(url);
			};

			button.recordRTC.startRecording();
			btnPauseRecording.style.display = '';
			recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '<img src="images/progress.gif">';
		};
	}

	if(recordingMedia.value === 'record-screen') {
		captureScreen(commonConfig);

		button.mediaCapturedCallback = function() {
			var options = {
			type: type,
				mimeType: mimeType,
				disableLogs: params.disableLogs || false,
				getNativeBlob: false, // enable it for longer recordings
				video: recordingPlayer
		};

			if(recorderType) {
				options.recorderType = recorderType;

				if(recorderType == WhammyRecorder || recorderType == GifRecorder || recorderType == WebAssemblyRecorder) {
					options.canvas = options.video = {
					width: defaultWidth || 320,
						height: defaultHeight || 240
				};
				}
			}

			if(videoBitsPerSecond) {
				options.videoBitsPerSecond = videoBitsPerSecond;
			}

			options.ignoreMutedMedia = false;
			button.recordRTC = RecordRTC(button.stream, options);

			button.recordingEndedCallback = function(url) {
				setVideoURL(url);
			};

			button.recordRTC.startRecording();
			btnPauseRecording.style.display = '';
			recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '<img src="images/progress.gif">';
		};
	}

	// note: audio+tab is supported in Chrome 50+
	// todo: add audio+tab recording
	if(recordingMedia.value === 'record-audio-plus-screen') {
		captureAudioPlusScreen(commonConfig);

		button.mediaCapturedCallback = function() {
			var options = {
			type: type,
				mimeType: mimeType,
				disableLogs: params.disableLogs || false,
				getNativeBlob: false, // enable it for longer recordings
				video: recordingPlayer
		};

			if(recorderType) {
				options.recorderType = recorderType;

				if(recorderType == WhammyRecorder || recorderType == GifRecorder || recorderType == WebAssemblyRecorder) {
					options.canvas = options.video = {
					width: defaultWidth || 320,
						height: defaultHeight || 240
				};
				}
			}

			if(videoBitsPerSecond) {
				options.videoBitsPerSecond = videoBitsPerSecond;
			}

			options.ignoreMutedMedia = false;
			button.recordRTC = RecordRTC(button.stream, options);

			button.recordingEndedCallback = function(url) {
				setVideoURL(url);
			};

			button.recordRTC.startRecording();
			btnPauseRecording.style.display = '';
			recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '<img src="images/progress.gif">';
		};
	}
};

function captureVideo(config) {
	captureUserMedia({video: true}, function(videoStream) {
		config.onMediaCaptured(videoStream);

		addStreamStopListener(videoStream, function() {
			config.onMediaStopped();
		});
	}, function(error) {
		config.onMediaCapturingFailed(error);
	});
}

function captureAudio(config) {
	captureUserMedia({audio: true}, function(audioStream) {
		config.onMediaCaptured(audioStream);

		addStreamStopListener(audioStream, function() {
			config.onMediaStopped();
		});
	}, function(error) {
		config.onMediaCapturingFailed(error);
	});
}

function captureAudioPlusVideo(config) {
	captureUserMedia({video: true, audio: true}, function(audioVideoStream) {
		config.onMediaCaptured(audioVideoStream);

		if(audioVideoStream instanceof Array) {
			audioVideoStream.forEach(function(stream) {
				addStreamStopListener(stream, function() {
					config.onMediaStopped();
				});
			});
			return;
		}

		addStreamStopListener(audioVideoStream, function() {
			config.onMediaStopped();
		});
	}, function(error) {
		config.onMediaCapturingFailed(error);
	});
}

var MY_DOMAIN = 'webrtc-experiment.com';

function isMyOwnDomain() {
	// replace "webrtc-experiment.com" with your own domain name
	return document.domain.indexOf(MY_DOMAIN) !== -1;
}

function isLocalHost() {
	// "chrome.exe" --enable-usermedia-screen-capturing
	// or firefox => about:config => "media.getusermedia.screensharing.allowed_domains" => add "localhost"
	return document.domain === 'localhost' || document.domain === '127.0.0.1';
}

var videoBitsPerSecond;

function setVideoBitrates() {
	var select = document.querySelector('.media-bitrates');
	var value = select.value;

	if(value == 'default') {
		videoBitsPerSecond = null;
		return;
	}

	videoBitsPerSecond = parseInt(value);
}

function getFrameRates(mediaConstraints) {
	if(!mediaConstraints.video) {
		return mediaConstraints;
	}

	var select = document.querySelector('.media-framerates');
	var value = select.value;

	if(value == 'default') {
		return mediaConstraints;
	}

	value = parseInt(value);

	if(DetectRTC.browser.name === 'Firefox') {
		mediaConstraints.video.frameRate = value;
		return mediaConstraints;
	}

	if(!mediaConstraints.video.mandatory) {
		mediaConstraints.video.mandatory = {};
		mediaConstraints.video.optional = [];
	}

	var isScreen = recordingMedia.value.toString().toLowerCase().indexOf('screen') != -1;
	if(isScreen) {
		mediaConstraints.video.mandatory.maxFrameRate = value;
	}
	else {
		mediaConstraints.video.mandatory.minFrameRate = value;
	}

	return mediaConstraints;
}

function setGetFromLocalStorage(selectors) {
	selectors.forEach(function(selector) {
		var storageItem = selector.replace(/\.|#/g, '');
		if(localStorage.getItem(storageItem)) {
			document.querySelector(selector).value = localStorage.getItem(storageItem);
		}

		addEventListenerToUploadLocalStorageItem(selector, ['change', 'blur'], function() {
			localStorage.setItem(storageItem, document.querySelector(selector).value);
		});
	});
}

function addEventListenerToUploadLocalStorageItem(selector, arr, callback) {
	arr.forEach(function(event) {
		document.querySelector(selector).addEventListener(event, callback, false);
	});
}

setGetFromLocalStorage(['.media-resolutions', '.media-framerates', '.media-bitrates', '.recording-media', '.media-container-format']);

function getVideoResolutions(mediaConstraints) {
	if(!mediaConstraints.video) {
		return mediaConstraints;
	}

	var select = document.querySelector('.media-resolutions');
	var value = select.value;

	if(value == 'default') {
		return mediaConstraints;
	}

	value = value.split('x');

	if(value.length != 2) {
		return mediaConstraints;
	}

	defaultWidth = parseInt(value[0]);
	defaultHeight = parseInt(value[1]);

	if(DetectRTC.browser.name === 'Firefox') {
		mediaConstraints.video.width = defaultWidth;
		mediaConstraints.video.height = defaultHeight;
		return mediaConstraints;
	}

	if(!mediaConstraints.video.mandatory) {
		mediaConstraints.video.mandatory = {};
		mediaConstraints.video.optional = [];
	}

	var isScreen = recordingMedia.value.toString().toLowerCase().indexOf('screen') != -1;

	if(isScreen) {
		mediaConstraints.video.mandatory.maxWidth = defaultWidth;
		mediaConstraints.video.mandatory.maxHeight = defaultHeight;
	}
	else {
		mediaConstraints.video.mandatory.minWidth = defaultWidth;
		mediaConstraints.video.mandatory.minHeight = defaultHeight;
	}

	return mediaConstraints;
}

function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
	if(mediaConstraints.video == true) {
		mediaConstraints.video = {};
	}

	setVideoBitrates();

	mediaConstraints = getVideoResolutions(mediaConstraints);
	mediaConstraints = getFrameRates(mediaConstraints);

	var isBlackBerry = !!(/BB10|BlackBerry/i.test(navigator.userAgent || ''));
	if(isBlackBerry && !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia)) {
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
		navigator.getUserMedia(mediaConstraints, successCallback, errorCallback);
		return;
	}

	navigator.mediaDevices.getUserMedia(mediaConstraints).then(function(stream) {
		successCallback(stream);

		setVideoURL(stream, true);
	}).catch(function(error) {
		if(error && (error.name === 'ConstraintNotSatisfiedError' || error.name === 'OverconstrainedError')) {
			alert('Your camera or browser does NOT supports selected resolutions or frame-rates. \n\nPlease select "default" resolutions.');
		}
		else if(error && error.message) {
			alert(error.message);
		}
		else {
			alert('Unable to make getUserMedia request. Please check browser console logs.');
		}

		errorCallback(error);
	});
}

function setMediaContainerFormat(arrayOfOptionsSupported) {
	var options = Array.prototype.slice.call(
		mediaContainerFormat.querySelectorAll('option')
	);

	var localStorageItem;
	if(localStorage.getItem('media-container-format')) {
		localStorageItem = localStorage.getItem('media-container-format');
	}

	var selectedItem;
	options.forEach(function(option) {
		option.disabled = true;

		if(arrayOfOptionsSupported.indexOf(option.value) !== -1) {
			option.disabled = false;

			if(localStorageItem && arrayOfOptionsSupported.indexOf(localStorageItem) != -1) {
				if(option.value != localStorageItem) return;
				option.selected = true;
				selectedItem = option;
				return;
			}

			if(!selectedItem) {
				option.selected = true;
				selectedItem = option;
			}
		}
	});
}

function isMimeTypeSupported(mimeType) {
	if(typeof MediaRecorder === 'undefined') {
		return false;
	}

	if(typeof MediaRecorder.isTypeSupported !== 'function') {
		return true;
	}

	return MediaRecorder.isTypeSupported(mimeType);
}

recordingMedia.onchange = function() {
	if(recordingMedia.value === 'record-audio') {
		var recordingOptions = [];

		if(isMimeTypeSupported('audio/webm')) {
			recordingOptions.push('opus');
		}

		if(isMimeTypeSupported('audio/ogg')) {
			recordingOptions.push('ogg');
		}

		recordingOptions.push('pcm');

		setMediaContainerFormat(recordingOptions);
		return;
	}

	var isChrome = !!window.chrome && !(!!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0);

	var recordingOptions = ['vp8']; // MediaStreamRecorder with vp8

	if(isMimeTypeSupported('video/webm\;codecs=vp9')) {
		recordingOptions.push('vp9'); // MediaStreamRecorder with vp9
	}

	if(isMimeTypeSupported('video/webm\;codecs=h264')) {
		recordingOptions.push('h264'); // MediaStreamRecorder with h264
	}

	if(isMimeTypeSupported('video/x-matroska;codecs=avc1')) {
		recordingOptions.push('mkv'); // MediaStreamRecorder with mkv/matroska
	}

	recordingOptions.push('gif'); // GifRecorder

	if(DetectRTC.browser.name == 'Chrome') {
		recordingOptions.push('whammy'); // WhammyRecorder
	}

	if(DetectRTC.browser.name == 'Chrome') {
		recordingOptions.push('WebAssembly'); // WebAssemblyRecorder
	}

	recordingOptions.push('default'); // Default mimeType for MediaStreamRecorder

	setMediaContainerFormat(recordingOptions);
};
recordingMedia.onchange();

if(typeof MediaRecorder === 'undefined' && (DetectRTC.browser.name === 'Edge' || DetectRTC.browser.name === 'Safari')) {
	// webp isn't supported in Microsoft Edge
	// neither MediaRecorder API
	// so lets disable both video/screen recording options

	console.warn('Neither MediaRecorder API nor webp is supported in ' + DetectRTC.browser.name + '. You cam merely record audio.');

	recordingMedia.innerHTML = '<option value="record-audio">Audio</option>';
	setMediaContainerFormat(['pcm']);
}

function stringify(obj) {
	var result = '';
	Object.keys(obj).forEach(function(key) {
		if(typeof obj[key] === 'function') {
			return;
		}

		if(result.length) {
			result += ',';
		}

		result += key + ': ' + obj[key];
	});

	return result;
}

function mediaRecorderToStringify(mediaRecorder) {
	var result = '';
	result += 'mimeType: ' + mediaRecorder.mimeType;
	result += ', state: ' + mediaRecorder.state;
	result += ', audioBitsPerSecond: ' + mediaRecorder.audioBitsPerSecond;
	result += ', videoBitsPerSecond: ' + mediaRecorder.videoBitsPerSecond;
	if(mediaRecorder.stream) {
		result += ', streamid: ' + mediaRecorder.stream.id;
		result += ', stream-active: ' + mediaRecorder.stream.active;
	}
	return result;
}

function getFailureReport() {
	var info = 'RecordRTC seems failed. \n\n' + stringify(DetectRTC.browser) + '\n\n' + DetectRTC.osName + ' ' + DetectRTC.osVersion + '\n';

	if (typeof recorderType !== 'undefined' && recorderType) {
		info += '\nrecorderType: ' + recorderType.name;
	}

	if (typeof mimeType !== 'undefined') {
		info += '\nmimeType: ' + mimeType;
	}

	Array.prototype.slice.call(document.querySelectorAll('select')).forEach(function(select) {
		info += '\n' + (select.id || select.className) + ': ' + select.value;
	});

	if (btnStartRecording.recordRTC) {
		info += '\n\ninternal-recorder: ' + btnStartRecording.recordRTC.getInternalRecorder().name;

		if(btnStartRecording.recordRTC.getInternalRecorder().getAllStates) {
			info += '\n\nrecorder-states: ' + btnStartRecording.recordRTC.getInternalRecorder().getAllStates();
		}
	}

	if(btnStartRecording.stream) {
		info += '\n\naudio-tracks: ' + getTracks(btnStartRecording.stream, 'audio').length;
		info += '\nvideo-tracks: ' + getTracks(btnStartRecording.stream, 'video').length;
		info += '\nstream-active? ' + !!btnStartRecording.stream.active;

		btnStartRecording.stream.getTracks().forEach(function(track) {
			info += '\n' + track.kind + '-track-' + (track.label || track.id) + ': (enabled: ' + !!track.enabled + ', readyState: ' + track.readyState + ', muted: ' + !!track.muted + ')';

			if(track.getConstraints && Object.keys(track.getConstraints()).length) {
				info += '\n' + track.kind + '-track-getConstraints: ' + stringify(track.getConstraints());
			}

			if(track.getSettings && Object.keys(track.getSettings()).length) {
				info += '\n' + track.kind + '-track-getSettings: ' + stringify(track.getSettings());
			}
		});
	}

	if(timeSlice && btnStartRecording.recordRTC) {
		info += '\ntimeSlice: ' + timeSlice;

		if(btnStartRecording.recordRTC.getInternalRecorder().getArrayOfBlobs) {
			var blobSizes = [];
			btnStartRecording.recordRTC.getInternalRecorder().getArrayOfBlobs().forEach(function(blob) {
				blobSizes.push(blob.size);
			});
			info += '\nblobSizes: ' + blobSizes;
		}
	}

	else if(btnStartRecording.recordRTC && btnStartRecording.recordRTC.getBlob()) {
		info += '\n\nblobSize: ' + bytesToSize(btnStartRecording.recordRTC.getBlob().size);
	}

	if(btnStartRecording.recordRTC && btnStartRecording.recordRTC.getInternalRecorder() && btnStartRecording.recordRTC.getInternalRecorder().getInternalRecorder && btnStartRecording.recordRTC.getInternalRecorder().getInternalRecorder()) {
		info += '\n\ngetInternalRecorder: ' + mediaRecorderToStringify(btnStartRecording.recordRTC.getInternalRecorder().getInternalRecorder());
	}

	return info;
}

function saveToDiskOrOpenNewTab(recordRTC) {
	if(!recordRTC.getBlob().size) {
		var info = getFailureReport();
		console.log('blob', recordRTC.getBlob());
		console.log('recordrtc instance', recordRTC);
		console.log('report', info);

		if(mediaContainerFormat.value !== 'default') {
			alert('RecordRTC seems failed recording using ' + mediaContainerFormat.value + '. Please choose "default" option from the drop down and record again.');
		}
		else {
			alert('RecordRTC seems failed. Unexpected issue. You can read the email in your console log. \n\nPlease report using disqus chat below.');
		}

		if(mediaContainerFormat.value !== 'vp9' && DetectRTC.browser.name === 'Chrome') {
			alert('Please record using VP9 encoder. (select from the dropdown)');
		}
	}

	var fileName = getFileName(fileExtension);

	document.querySelector('#save-to-disk').parentNode.style.display = 'block';
	document.querySelector('#save-to-disk').onclick = function() {
		if(!recordRTC) return alert('No recording found.');

		var file = new File([recordRTC.getBlob()], fileName, {
		type: mimeType
		});

		invokeSaveAsDialog(file, file.name);
	};

	document.querySelector('#open-new-tab').onclick = function() {
		if(!recordRTC) return alert('No recording found.');

		var file = new File([recordRTC.getBlob()], fileName, {
		type: mimeType
		});

		window.open(URL.createObjectURL(file));
	};

	// upload to PHP server
	document.querySelector('#upload-to-php').disabled = false;

	document.querySelector('#upload-to-php').onclick = function() {
		if(!recordRTC) return alert('No recording found.');
		this.disabled = true;

		var button = this;
		uploadToPHPServer(fileName, recordRTC, function(progress, fileURL, playerURL) {
			if(progress === 'ended') {
				button.disabled = false;
				button.innerHTML = 'Ré-envoyer sur le serveur UTC';

				setVideoURL(fileURL);

				recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '';
				var html = '<p>La vidéo a été envoyée sur le serveur</p>';
				html += '<p>Vous pouvez <a href="'+playerURL+'" target="_blank">tester le lien</a><br>';
				html += '<p>Ou le copier ci-dessous :</p><p><span class="smalllink">' + window.location.href + playerURL + '</span></p><p>Vous pouvez par exemple mettre ce lien dans votre cours moodle ou le communiquer aux étudiants.</p><p>Pour certains formats de fichier, il est nécessaire d\'attendre quelques dizaines de minutes avant de pouvoir les lire (temps de ré-encodage)</p>';
				showmodal(html);

				return;
			}
			button.innerHTML = progress;
			recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = progress;
		});
	};
}



function handleExternalupload(f) {
	var file = f.item(0);
	var extension = file.name.split('.').pop();
	var fileName = 'RecordRTC-'+Math.random().toString(36).substring(2, 15)+'.'+extension;
	var button = document.querySelector("#upload-to-php");
	uploadToPHPServer(fileName, file, function(progress, fileURL, playerURL) {
		if(progress === 'ended') {
			button.disabled = false;
			button.innerHTML = 'Ré-envoyer sur le serveur UTC';

			setVideoURL(fileURL);

			recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '';
			var html = '<p>La vidéo a été envoyée sur le serveur</p>';
			html += '<p>Vous pouvez <a href="'+playerURL+'" target="_blank">tester le lien</a><br>';
			html += '<p>Ou le copier ci-dessous :</p><p><span class="smalllink">' + window.location.href + playerURL + '</span></p><p>Vous pouvez par exemple mettre ce lien dans votre cours moodle ou le communiquer aux étudiants.</p><p>Pour certains formats de fichier, il est nécessaire d\'attendre quelques dizaines de minutes avant de pouvoir les lire (temps de ré-encodage)</p>';
			showmodal(html);

			return;
		}
		button.innerHTML = progress;
		recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = progress;
	});
	
}
function uploadToPHPServer(fileName, recordRTC, callback) {
	var blob = null;

	// type file = direct file upload
	if (recordRTC instanceof File) {
		blob = recordRTC;
	// other type = recording
	} else {
		blob = recordRTC instanceof Blob ? recordRTC : recordRTC.getBlob();

		blob = new File([blob], getFileName(fileExtension), {
			type: mimeType
		});
	}

	// create FormData
	var formdata = new FormData();
	formdata.append('videofilename', fileName);
<?php
	if (!$anonymous) {
echo <<<WRITEJS
	formdata.append('user', '$login');
	formdata.append('mail', '$email_address');
	formdata.append('hash', '$hash');
WRITEJS;
	}
?>
	formdata.append('videoblob', blob);

	callback('Uploading recorded-file to server.');

	// var upload_url = 'https://your-domain.com/files-uploader/';
	var upload_url = 'save.php';

	// var upload_directory = upload_url;
	var upload_directory = 'uploads/';

	makeXMLHttpRequest(upload_url, formdata, function(progress) {
		if (progress !== 'upload-ended') {
			callback(progress);
			return;
		}

		callback('ended', upload_directory + fileName, "player.php?name="+fileName.split(".")[0]);
	});
}

function makeXMLHttpRequest(url, data, callback) {
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			if(request.responseText === 'success') {
				callback('upload-ended');
				return;
			}

			document.querySelector('.header').parentNode.style = 'text-align: left; color: red; padding: 5px 10px;';
			document.querySelector('.header').parentNode.innerHTML = request.responseText;
		}
	};

	request.upload.onloadstart = function() {
		callback('Upload started...');
	};

	request.upload.onprogress = function(event) {
		callback('Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%");
	};

	request.upload.onload = function() {
		callback('progress-about-to-end');
	};

	request.upload.onload = function() {
		callback('Getting File URL..');
	};

	request.upload.onerror = function(error) {
		callback('Failed to upload to server');
	};

	request.upload.onabort = function(error) {
		callback('Upload aborted.');
	};

	request.open('POST', url);
	request.send(data);
}

function getRandomString() {
	if (window.crypto && window.crypto.getRandomValues && navigator.userAgent.indexOf('Safari') === -1) {
		var a = window.crypto.getRandomValues(new Uint32Array(3)),
			token = '';
		for (var i = 0, l = a.length; i < l; i++) {
			token += a[i].toString(36);
		}
		return token;
	} else {
		return (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
	}
}

function getFileName(fileExtension) {
	var d = new Date();
	var year = d.getUTCFullYear();
	var month = d.getUTCMonth();
	var date = d.getUTCDate();
	return 'RecordRTC-' + year + month + date + '-' + getRandomString() + '.' + fileExtension;
}

function SaveFileURLToDisk(fileUrl, fileName) {
	var hyperlink = document.createElement('a');
	hyperlink.href = fileUrl;
	hyperlink.target = '_blank';
	hyperlink.download = fileName || fileUrl;

	(document.body || document.documentElement).appendChild(hyperlink);
	hyperlink.onclick = function() {
		(document.body || document.documentElement).removeChild(hyperlink);

		// required for Firefox
		window.URL.revokeObjectURL(hyperlink.href);
	};

	var mouseEvent = new MouseEvent('click', {
	view: window,
		bubbles: true,
		cancelable: true
	});

	hyperlink.dispatchEvent(mouseEvent);
}

function getURL(arg) {
	var url = arg;

	if(arg instanceof Blob || arg instanceof File) {
		url = URL.createObjectURL(arg);
	}

	if(arg instanceof RecordRTC || arg.getBlob) {
		url = URL.createObjectURL(arg.getBlob());
	}

	if(arg instanceof MediaStream || arg.getTracks) {
		// url = URL.createObjectURL(arg);
	}

	return url;
}

function setVideoURL(arg, forceNonImage) {
	var url = getURL(arg);

	var parentNode = recordingPlayer.parentNode;
	parentNode.removeChild(recordingPlayer);
	parentNode.innerHTML = '';

	var elem = 'video';
	if(type == 'gif' && !forceNonImage) {
		elem = 'img';
	}
	if(type == 'audio') {
		elem = 'audio';
	}

	recordingPlayer = document.createElement(elem);

	if(arg instanceof MediaStream) {
		recordingPlayer.muted = true;
	}

	recordingPlayer.addEventListener('loadedmetadata', function() {
		if(navigator.userAgent.toLowerCase().indexOf('android') == -1) return;

		// android
		setTimeout(function() {
			if(typeof recordingPlayer.play === 'function') {
				recordingPlayer.play();
			}
		}, 2000);
	}, false);

	recordingPlayer.poster = '';

	if(arg instanceof MediaStream) {
		recordingPlayer.srcObject = arg;
	}
	else {
		recordingPlayer.src = url;
	}

	if(typeof recordingPlayer.play === 'function') {
		recordingPlayer.play();
	}

	recordingPlayer.addEventListener('ended', function() {
		url = getURL(arg);

		if(arg instanceof MediaStream) {
			recordingPlayer.srcObject = arg;
		}
		else {
			recordingPlayer.src = url;
		}
	});

	parentNode.appendChild(recordingPlayer);
}
</script>

<script>
function captureScreen(config) {
	if (navigator.getDisplayMedia) {
		navigator.getDisplayMedia(vconstraints).then(screenStream => {
	config.onMediaCaptured(screenStream);

	addStreamStopListener(screenStream, function() {
		// config.onMediaStopped();

		btnStartRecording.onclick();
	});

	setVideoURL(screenStream, true);
	}).catch(function(error) {
		config.onMediaCapturingFailed(error);
	});
	} else if (navigator.mediaDevices.getDisplayMedia) {
		navigator.mediaDevices.getDisplayMedia(vconstraints).then(screenStream => {
	config.onMediaCaptured(screenStream);

	addStreamStopListener(screenStream, function() {
		// config.onMediaStopped();

		btnStartRecording.onclick();
	});

	setVideoURL(screenStream, true);
	}).catch(function(error) {
		config.onMediaCapturingFailed(error);
	});
	} else {
		var error = 'getDisplayMedia API are not supported in this browser.';
		config.onMediaCapturingFailed(error);
		alert(error);
	}
}

function captureAudioPlusScreen(config) {
	if (navigator.getDisplayMedia) {
		navigator.getDisplayMedia(vconstraints).then(screenStream => {
			navigator.mediaDevices.getUserMedia({audio:true}).then(function(mic) {
				screenStream.addTrack(mic.getTracks()[0]);

				config.onMediaCaptured(screenStream);

				addStreamStopListener(screenStream, function() {
				// config.onMediaStopped();

					btnStartRecording.onclick();
				});

				setVideoURL(screenStream, true);
			}).catch(function(error) {
				config.onMediaCapturingFailed(error);
			});
		}).catch(function(error) {
			config.onMediaCapturingFailed(error);
		});
	} else if (navigator.mediaDevices.getDisplayMedia) {
		navigator.mediaDevices.getDisplayMedia(vconstraints).then(screenStream => {
			navigator.mediaDevices.getUserMedia({audio:true}).then(function(mic) {
				screenStream.addTrack(mic.getTracks()[0]);

				config.onMediaCaptured(screenStream);

				addStreamStopListener(screenStream, function() {
					// config.onMediaStopped();
					btnStartRecording.onclick();
				});
				setVideoURL(screenStream, true);
			}).catch(function(error) {
				config.onMediaCapturingFailed(error);
			});
		}).catch(function(error) {
			config.onMediaCapturingFailed(error);
		});
	} else {
		var error = 'getDisplayMedia API are not supported in this browser.';
		config.onMediaCapturingFailed(error);
		alert(error);
	}
}
</script>

<script>
</script>

<script>
var chkFixSeeking = document.querySelector('#chk-fixSeeking');
chkFixSeeking.onchange = function() {
	if(this.checked === true) {
		localStorage.setItem(this.id, 'true');
	}
	else {
		localStorage.removeItem(this.id);
	}
};
if(localStorage.getItem(chkFixSeeking.id) === 'true') {
	chkFixSeeking.checked = true;
}
</script>

<script>
var chkTimeSlice = document.querySelector('#chk-timeSlice');
var timeSlice = false;

if(typeof MediaRecorder === 'undefined') {
	chkTimeSlice.disabled = true;
}

chkTimeSlice.addEventListener('change', function() {
	if(chkTimeSlice.checked === true) {
		var _timeSlice = prompt('Please enter timeSlice in milliseconds e.g. 1000 or 2000 or 3000.', 1000);
		_timeSlice = parseInt(_timeSlice);
		if(!_timeSlice || _timeSlice == NaN || typeof _timeSlice === 'undefined') {
			timeSlice = false;
			return;
		}

		timeSlice = _timeSlice;
	}
	else {
		timeSlice = false;
	}
}, false);
</script>

<script>
var btnPauseRecording = document.querySelector('#btn-pause-recording');
btnPauseRecording.onclick = function() {
	if(!btnStartRecording.recordRTC) {
		btnPauseRecording.style.display = 'none';
		return;
	}

	btnPauseRecording.disabled = true;
	if(btnPauseRecording.innerHTML === 'Pause') {
		btnStartRecording.disabled = true;
		chkFixSeeking.parentNode.style.display = 'none';
		btnStartRecording.style.fontSize = '15px';
		btnStartRecording.recordRTC.pauseRecording();
		recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = 'Enregistrement : en pause';
		recordingPlayer.pause();

		btnPauseRecording.style.fontSize = 'inherit';
		setTimeout(function() {
			btnPauseRecording.innerHTML = 'Resume Recording';
			btnPauseRecording.disabled = false;
		}, 2000);
	}

	if(btnPauseRecording.innerHTML === 'Resume Recording') {
		btnStartRecording.disabled = false;
		chkFixSeeking.parentNode.style.display = 'none';
		btnStartRecording.style.fontSize = 'inherit';
		btnStartRecording.recordRTC.resumeRecording();
		recordingPlayer.parentNode.parentNode.querySelector('h2').innerHTML = '<img src="images/progress.gif">';
		recordingPlayer.play();

		btnPauseRecording.style.fontSize = '15px';
		btnPauseRecording.innerHTML = 'Pause';
		setTimeout(function() {
			btnPauseRecording.disabled = false;
		}, 2000);
	}
};

function closemodal() {
	document.querySelector("#status").className = "experiment";
}

function renamevid(id) {
	if (title=prompt("Entrez le nouveau nom",""))
	{
		var request = new XMLHttpRequest();
		var formdata = new FormData();
<?php
	if (!$anonymous) {
echo <<<WRITEJS
	formdata.append('user', '$login');
	formdata.append('mail', '$email_address');
	formdata.append('hash', '$hash');
WRITEJS;
	}
?>
		formdata.append('id', id)
		formdata.append('title', title)
		request.open('POST', 'renamevid.php');
		request.send(formdata);
		request.onreadystatechange = function() {
			if (request.readyState == 4) {
				id=formdata.get('id');
				if (request.status == 200) {
					title=formdata.get('title');
					addstatustext('<p>Titre modifié pour la vidéo "'+title+'"  !</p>');
					e=document.getElementById('videorow-'+id);
					if (e) {
						e.getElementsByTagName('a')[0].innerHTML=title;
					}
				} else if (request.status >= 400) {
					var html = '<p>Une erreur a été rencontrée lors du changement de titre de la vidéo "'+title+'"</p>';
					html += '<p>'+Request.responseText+'</p>';
					showmodal(html);
				}
			}
		};
	}
}

function deletevid(id, filename) {
	title=filename;
	e=document.getElementById('videorow-'+id);
	if (e) {
		title=e.getElementsByTagName('a')[0].innerHTML;
	}
	if (confirm("Voulez vous supprimer la vidéo "+title+" ? Elle sera immédiatement innaccessible et supprimée du serveur après 2 semaines."))
	{
		var request = new XMLHttpRequest();
		var formdata = new FormData();
<?php
	if (!$anonymous) {
echo <<<WRITEJS
	formdata.append('user', '$login');
	formdata.append('mail', '$email_address');
	formdata.append('hash', '$hash');
WRITEJS;
	}
?>
		formdata.append('id', id);
		formdata.append('filename', filename);
		formdata.append('title', title);
		request.open('POST', 'deletevid.php');
		request.onreadystatechange = function() {
			if (request.readyState == 4) {
				id=formdata.get('id');
				if (request.status == 200) {
					addstatustext('<p>Vidéo "'+formdata.get('title')+'" supprimée !</p>');
					e=document.getElementById('videorow-'+id);
					if (e) {
						deleted_vids = document.getElementById('deleted_vids');
						row = deleted_vids.insertRow();
						row.id='deletedvideorow-'+id;
						action_cell = row.insertCell();
						action_cell.classList.add('rowaction');
						undo_img = document.createElement("img");
						undo_img.src='images/undo.svg';
						undo_img.width='24';
						undo_img.height='24';
						undo_img.id='btn-restore-file';
						undo_img.setAttribute('onclick','restorevid('+id+', \''+filename+'\')');
						undo_img.alt='Restaurer la vidéo '+title;
						undo_img.title='Restaurer la vidéo '+title;
						action_cell.appendChild(undo_img);
						title_cell = row.insertCell();
						a = document.createElement("a");
						a.textContent = title;
						title_cell.appendChild(a);
						date_cell = row.insertCell();
						date_cell.classList.add('timestamp');
						date_cell.textContent = e.cells[2].textContent;
						deleted_date_cell = row.insertCell();
						e.remove();
					}
				} else if (request.status >= 400) {
					var html = '<p>Une erreur a été rencontrée lors de la suppression de la vidéo "'+title+'" ('+filename+') (contactez la CAP pour étudier le problème) :</p>';
					html += '<p>'+Request.responseText+'</p>';
					showmodal(html);
				}
			}
		};
		request.send(formdata);
	}
}

function restorevid(id, filename, name_in_url) {
	title=filename;
	e=document.getElementById('deletedvideorow-'+id);
	if (e) {
		title=e.getElementsByTagName('a')[0].innerHTML;
	}
	if (confirm("Voulez vous restaurer la vidéo "+title+" ?"))
	{
		var request = new XMLHttpRequest();
		var formdata = new FormData();
<?php
	if (!$anonymous) {
echo <<<WRITEJS
	formdata.append('user', '$login');
	formdata.append('mail', '$email_address');
	formdata.append('hash', '$hash');
WRITEJS;
	}
?>
		formdata.append('id', id);
		formdata.append('filename', filename);
		formdata.append('title', title);
		request.open('POST', 'restorevid.php');
		request.onreadystatechange = function() {
			if (request.readyState == 4) {
				id=formdata.get('id');
				if (request.status == 200) {
					addstatustext('<p>Vidéo "'+formdata.get('title')+'" restaurée !</p>');
					e=document.getElementById('deletedvideorow-'+id);
					if (e) {
						sent_vids = document.getElementById('sent_vids');
						row = sent_vids.insertRow();
						row.id='videorow-'+id;
						action_cell = row.insertCell();
						action_cell.classList.add('rowaction');
						rename_img = document.createElement("img");
						rename_img.src='images/rename.svg';
						rename_img.width='24';
						rename_img.height='24';
						rename_img.id='btn-rename-file';
						rename_img.setAttribute('onclick','renamevid('+id+')');
						rename_img.alt='Changer le titre de la vidéo '+title;
						rename_img.title='Changer le titre de la vidéo '+title;
						action_cell.appendChild(rename_img);
						delete_img = document.createElement("img");
						delete_img.src='images/delete.svg';
						delete_img.width='24';
						delete_img.height='24';
						delete_img.id='btn-delete-file';
						delete_img.setAttribute('onclick','deletevid('+id+',\''+filename+'\')');
						delete_img.alt='Supprimer la vidéo '+title;
						delete_img.title='Supprimer la vidéo '+title;
						action_cell.appendChild(delete_img);
						title_cell = row.insertCell();
						a = document.createElement("a");
						a.textContent = title;
						a.href= 'player.php?name='+name_in_url;
						a.target= '_blank';
						title_cell.appendChild(a);
						date_cell = row.insertCell();
						date_cell.classList.add('timestamp');
						date_cell.textContent = e.cells[2].textContent;
						e.remove();
					}
				} else if (request.status >= 400) {
					var html = '<p>Une erreur a été rencontrée lors de la restauration de la vidéo "'+title+'" ('+filename+') (contactez la CAP pour étudier le problème) :</p>';
					html += '<p>'+Request.responseText+'</p>';
					showmodal(html);
				}
			}
		};
		request.send(formdata);
	}
}
</script>
	<section id="status" class="experiment">
		<a id="closemodal" href="javascript:void(0)" onclick="closemodal();"><img src="images/close.svg" alt="Fermer cette fenêtre" height="32" width="32"/></a>
		<div id="statusco"> &nbsp; </div>
        </section>

	<section id="video_list" class="experiment">
		<div id="list">
			<h1>Vidéos envoyées</h1>
			<?php
				$db=new Db;
				$results = $db->getvidsbyuser($login);
			?>
			<table id="sent_vids">
				<tr>
					<th>Actions</th>
					<th>Titre</th>
					<!--th>Taille</th-->
					<th>Date de création</th>
				</tr>
			<?php
				foreach ($results as &$row) {
					if (!$row["deleted"]) {
                                                $name_in_url = pathinfo($row["filename"])['filename'];
						if ($row["title"])
							$displaytitle=$row["title"];
						else
							$displaytitle=$row["filename"];
						$displaytitle=dbescapehtml($displaytitle);
						echo '<tr id="videorow-'.$row["id"].'"><td class="rowaction">';
						echo '<img src="images/rename.svg" width="24" height="24" id="btn-rename-file" onclick="renamevid('.$row["id"].')" alt="Changer le titre de la vidéo '.$displaytitle.'" title="Changer le titre de la vidéo '.$displaytitle.'">';
						echo '<img src="images/delete.svg" width="24" height="24" id="btn-delete-file" onclick="deletevid('.$row["id"].', \''.$row["filename"].'\')" alt="Supprimer la vidéo '.$displaytitle.'" title="Supprimer la vidéo '.$displaytitle.'">';
						echo '</td><td><a target="_blank" href="player.php?name='.$name_in_url.'">';
						echo $displaytitle;
						echo '</a>';
						echo '</td><td class="timestamp">';
						echo $row["ts"];
						echo "</td>";
					}
				}
			?>
			</table>
		</div>
		<div>
			<p>Explications :</p>
			<ul class="features">
				<li><img src="images/rename.svg" width="24" height="24"> : Changer le titre d'une vidéo. Pour votre propre organisation (le nom du fichier et le lien ne changent pas).</li>
				<li><img src="images/delete.svg" width="24" height="24"> : Supprimer une vidéo. elle sera immédiatement inaccessible, et effacée définitivement des serveurs sous 2 semaines.</li>
				<li>Lien : Ouvrir dans la page web de lecture. Vous pouvez donner cette adresse aux étudiants.</li>
			</ul>
		</div>
        </section>

	<section id="trash" class="experiment">
		<details>
		<summary>
			<h1>Corbeille</h1>
			<!--img src="images/arrow-down.svg" width="15" height="15" id="btn-show-trash" onclick="showblock()"-->
		</summary>
		<div id="trash_list">
			<?php
				$db=new Db;
				$results = $db->getvidsbyuser($login);
			?>
			<table id="deleted_vids">
					<th>Actions</th>
					<th>Titre</th>
					<!--th>Taille</th-->
					<th>Création</th>
					<th>Suppression</th>
				</tr>
			<?php
				foreach ($results as &$row) {
					if ($row["deleted"]) {
                                                $name_in_url = pathinfo($row["filename"])['filename'];
						if ($row["title"])
							$displaytitle=$row["title"];
						else
							$displaytitle=$row["filename"];
						$displaytitle=dbescapehtml($displaytitle);
						echo '<tr id="deletedvideorow-'.$row["id"].'"><td class="rowaction">';
						echo '<img src="images/undo.svg" width="24" height="24" id="btn-restore-file" onclick="restorevid('.$row["id"].', \''.$row["filename"].'\', \''.$name_in_url.'\')" alt="Restaurer la vidéo '.$displaytitle.'" title="Restaurer la vidéo '.$displaytitle.'">';
						echo '</td><td><a>';
						echo $displaytitle;
						echo '</a></td>';
						echo '<td class="timestamp">';
						echo $row["ts"];
						echo "</td>";
						echo '<td class="timestamp">';
						echo $row["deleted"];
						echo "</td>";
					}
				}
			?>
			</table>
			<div>
				<p>Explications :</p>
				<ul class="features">
					<li><img src="images/undo.svg" width="24" height="24"> : Restaurer une vidéo.</li>
				</ul>
			</div>
		</div>
		</details>
        </section>
        <section class="experiment credits">
            <p style="margin-top: 6px; text-align: center;">
	        <a href="privacy.html">Conditions d’utilisations et traitement des données personnelles</a><br>
	        <a href="https://github.com/muaz-khan/RecordRTC">Basé sur un logiciel libre : RecordRTC de @muaz-khan</a><br>
                Modifié par la <a href="mailto:cap@utc.fr">Cellule d'Appui Pédagogique</a> ; Accès au <a href="https://gitlab.utc.fr/spoinsar/course-record" target="_blank">code source</a> ; <a href="contributors.html">contributeurs</a><br>
                <img src="images/logo-feder-small.jpg" alt="L'Europe s'engage en Hauts-de-France avec le Feder ; Mission Numérique">
            </p>
        </section>
    </article>
<script>
function isGoogleChrome() {
	// please note,
	// that IE11 now returns undefined again for window.chrome
	// and new Opera 30 outputs true for window.chrome
	// but needs to check if window.opr is not undefined
	// and new IE Edge outputs to true now for window.chrome
	// and if not iOS Chrome check
	// so use the below updated condition
	var winNav = window.navigator;
	var vendorName = winNav.vendor;
	var isOpera = typeof window.opr !== "undefined";
	var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
	var isIOSChrome = winNav.userAgent.match("CriOS");

	return (!isIOSChrome &&
	  vendorName === "Google Inc." &&
	  isOpera === false &&
	  isIEedge === false
 	);
}
if (!isGoogleChrome()) {
	document.getElementById('warnchrome').style.display='inherit';
}
</script>
</body>

</html>
