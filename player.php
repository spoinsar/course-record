<?php
require_once 'inc/config.php';
require_once '3rdparty/phpCAS/CAS.php';
require_once 'inc/database.php';
// cas connection
PhpCas::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);
phpCAS::setNoCasServerValidation();
if ($_GET['code']) {
	$sslkey=hash('sha512', constant('SECRET_UPLOAD_HASH'));
	$ssliv=substr($sslkey, -16);
	$sslkey=substr($sslkey, 0, 32);
	if ($_GET['crypt'] && $_GET['authlevel']) {
		$cryptedMessage = urlencode(base64_encode(openssl_encrypt($_GET['authlevel'] . $_GET['code'], "aes-256-cbc", $sslkey, 0, $ssliv)));
		echo "Crypted code part of the URL: $cryptedMessage";
		die();
	}
	$decryptedMessage = openssl_decrypt(base64_decode($_GET['code']), "AES-256-CBC", $sslkey, 0, $ssliv);
	if (is_numeric($decryptedMessage[0])) {
		$_GET['name']=substr($decryptedMessage, 1);
		$authlevel=(int) $decryptedMessage[0];
		// echo "name : ".$_GET['name'].", authlevel: ".$authlevel;
	} else {
		echo "erreur d'accès, le code de la vidéo n'a pas pu être traité";
		die();
	}
}
// get login user cas if is he connected
$is_cas_user = PhpCas::checkAuthentication();
if($is_cas_user || $_GET['forceauth'] || $authlevel) {
    phpCAS::forceAuthentication();
    $login_user = PhpCas::getUser();
    if ($authlevel>=2) {
	    $cas_attributes=phpCAS::getAttributes();
	    if ($cas_attributes['accountProfile']!='utc-pers') {
		    $_GET['name']=null;
		    $video_object->title="Echec de lecture : vidéo réservée au personnel UTC";
            }
    }
}else{
    $login_user = null;
}
$video_object = null;
$tc = null;
$end = null;
$start = null;
if (isset($_GET['name'])) {
    // get name video file and add dot at the end
    $name_video = $_GET['name'] . '.%';
    $db = new Db();
    // get video information
    $video_object = $db->getvid($name_video);
    // if video exist and haven't title, create title with filename
    if($video_object != null && $video_object->title == null){
//        $file_name_explode = explode('.', $video_object->filename);
//        array_pop($file_name_explode);
//        $video_object->title = implode(' ', $file_name_explode);
        $video_object->title = "Vidéo pédagogique";
    }
    // get time code if exist
    if(isset($_GET['tc'])){
        $tc = $_GET['tc'];
    }
    // get offsets if exist
    if(isset($_GET['start'])){
        $start = $_GET['start'];
    }
    if(isset($_GET['end'])){
        $end = $_GET['end'];
    }
}
?>
<html>
    <head>
        <title><?= ($video_object != null) ? $video_object->title : 'vidéo non trouvé' ?></title>
        <meta charset="UTF-8">
        <link href="3rdparty/videojs/video-js.css" rel="stylesheet" type="text/css"/>
        <style>
            header{
                text-align: center;
            }
            h1{
                display: inline;
                border-bottom: 1px solid rgb(189, 189, 189);
            }
            .video-js .vjs-big-play-button {
                left: 42.5%;
                top: 42.5%;
                width: 15%;
                height: 15%;
            }
            .video-js{
                display: block;
                margin: 0 auto;
                margin-top: 50px;
            }
            #tc, #offset_start, #offset_end{
                display: none;
            }
            #recap_list, #edit_menu{
                margin-top: 30px;
                display: flex;
                flex-direction: row;
                list-style: none;
                align-items: center;
                justify-content: space-around;
            }
            #modal_content div{
                text-align: center;
            }
            
            button{
                color: #fff;
                background-color: #017ff4;
                border-color: #076bd2;
                border-radius: 3px;
                font-size: 1.4em;
            }
            
            #error_msg, #error_edit{
                color: red;
                font-weight: bold;
            }
            #response_edit, #recap_edit, #tuto_edit, #title_edit_menu{
                margin-top: 30px;
                text-align: center;
                border-top: 1px solid rgb(189, 189, 189);
            }
            #btn_tuto{
                list-style: none;
            }
            #body_tuto p{
                margin-left: 40px;
            }
            h3{
                margin-top: 30px;
                text-decoration: underline;
	    }
	    #edit_link {
	        font-size:2em;
	    }
	    .mainerror {
                display:block;
                color:red;
		text-align:center;
                font-weight:bold;
                font-size:1.6em;
	    }
	    .videosize {
                position:absolute;
                right:0.4em;
                top:0.4em;
		color:#888888;
                font-size:0.9em;
            }
        </style>
    </head>
    <body>
        <?php if($video_object != null){ ?>
        <!-- values of timecode and offsets for js in hidden html-->
        <span id="tc"><?= $tc ?></span>
        <span id="offset_start"><?= $start ?></span>
        <span id="offset_end"><?= $end ?></span>
        <header>
		<h1><?= $video_object->title ?></h1>
		<meta name="robots" content="noindex">
	</header>
<?php
	$sourceorigin="";
	$sourceeco="";
	$fileorigin="uploads/".$video_object->filename;
	$fileeco="v/".pathinfo($video_object->filename)['filename'].'-eco.mp4';
	$sizeorigin=null;
	$sizeeco=null;
	$needencode=false;
	if (file_exists($fileorigin)) {
		$fileoriginext=strtolower(pathinfo($fileorigin)['extension']);
		if ($fileoriginext=='webm' || $fileoriginext=='mp4') {
			$sourceorigin='<source src="'.$fileorigin.'">'."\n";
		} else {
			$needencode=true;
		}
		$sizeorigin=filesize($fileorigin);
	}
	if (file_exists($fileeco) && (filemtime($fileeco)+15<time())) {
		$sourceeco='<source src="'.$fileeco.'" type="video/mp4">'."\n";
		$sizeeco=filesize($fileeco);
		$needencode=false;
	}
	if ($_GET['origin']) {
		$sourcesall="$sourceorigin$sourceeco";
		$sizevideo=$sizeorigin;
		if (!$sizevideo) {
			$sizevideo=$sizeeco;
		}
	} else {
		$sourcesall="$sourceeco$sourceorigin";
		$sizevideo=$sizeeco;
		if (!$sizevideo) {
			$sizevideo=$sizeorigin;
		}
	}
	$sizevideo=round((float)$sizevideo/1024.0/1024.0, 2);
	$sizepercent="";
	if ($sizeorigin && $sizeeco) {
		$sizepercent=" (".round((float)$sizeeco*100.0/(float)$sizeorigin, 1).'%)';
	}
	if ($sourcesall=="") {
		if ($needencode) {
			echo '<p class="mainerror">Le serveur n\'a pas encore encodé cette vidéo. Ce processus peut prendre quelques dizaines de minutes. Revenez sur cette page prochainement.</p>';
		} else {
			echo '<p class="mainerror">Vidéo introuvable... (supprimée ou en erreur)</p>';
		}
	} else {
		echo '<div class="videosize">'.$sizevideo.' MB'.$sizepercent.'</div>';
	}

?>
        <div id="video_container">
            <video
                id="my_video"
                class="video-js"
                controls
                width="1080"
                height="720"
                data-setup='{ "playbackRates": [0.5, 1, 1.25, 1.5, 1.75, 2] }'
		>
		<?php
			echo $sourcesall;
		?>
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a
                    web browser that
                    <a href="https://videojs.com/html5-video-support/" target="blank">supports HTML5 video</a>
                </p>
            </video>
        </div>
	<?php if($login_user != null && $login_user == $video_object->user){?>
	<br>
	<details>
	<summary>Modifier les options de la page de lecture (réglage enseignant)</summary>
	<div id="title_edit_menu">
	    <h2>Modifier les options de la page de lecture</h2>
		<p>Avant de partager le lien, vous pouvez choisir des options pour couper le début ou la fin de la vidéo lorsqu'elle sera affichée sur la page de lecture.</p>
		<p>Mettez en pause le lecteur vidéo à l'instant de votre choix et cliquez sur un l'un des boutons suivants :</p>
        </div>
        <div id="edit_menu">
            <button id="set_start">Couper le début</button>
            <button id="set_tc">Commencer la lecture à...</button>
            <button id="set_end">Couper la fin</button>
        </div>
        <div id="recap_edit">
            <h2>Récapitulatif des modifications</h2>
            <ul id="recap_list">
                <li><strong>Couper le début : </strong><span id="get_start">non modifé</span></li>
                <li><strong>Commencer à... : </strong><span id="get_tc">non renseigné</span></li>
                <li><strong>Couper la fin : </strong><span id="get_end">non modifé</span></li>
            </ul>
        </div> 
        <div id="response_edit">
            <h2 id="error_edit"></h2>
	    <h2>Lien de partage de la video</h2>
		<p>Copiez le lien suivant et diffusez le à vos étudiants pour donner une nouvelle version de la page de lecture qui prend en compte vos choix :</p>
		<div><a id="edit_link" target="blank"></a></div>
		<p>Le fichier n'étant pas modifié directement, si la vidéo est téléchargée elle contiendra toujours les parties coupées, cette méthode n'est pas efficace pour retirer des informations confidentielles de la vidéo mais juste pour faciliter la vie des étudiants en évitant de leur montrer des séquences peu intéressantes.</p>
        </div>
        <div id="tuto_edit">
            <h2>Guide des utilisations</h2>
        </div>
        <div id="body_tuto">
            <h3>Explication des boutons de contrôle</h3>
            <ul id="btn_tuto">
                <li><strong>Couper le début : </strong>La lecture commencera au moment choisi. La partie coupée n'apparaitra pas dans la barre de lecture, l'étudiant ne pourra pas directement afficher la partie coupée de la vidéo.</li>
                <li><strong>Commencer la lecture à... : </strong>La lecture commencera au moment choisi, mais la partie précédente de la vidéo apparaitra dans la barre de lecture, l'étudiant pourra revenir en arrière facilement.</li>
                <li><strong>Couper la fin : </strong>La vidéo se terminera au moment choisi. La partie coupée n'apparaitra pas dans la barre de lecture, l'étudiant ne pourra pas directement afficher la partie coupée de la vidéo..</li>
            </ul>
            <p>Vous pouvez combinez tous les boutons ensemble ou en utiliser qu'un.</p>
            <h3>Explication liens de partage de la video</h3>
            <p>Liens de partage de votre vidéo éditée, clic droit + copier le lien pour le partager ou cliquer dessus pour tester vos modifications.</p>
	</div>
	</details>
        <?php }
         } else { ?>
        <h1>Cette vidéo n'existe pas</h1>
            <?php } ?>
        <script src="3rdparty/videojs/video.min.js" type="text/javascript"></script>
	<script src="3rdparty/videojs/videojs-offset.min.js" type="text/javascript"></script>

	<script type="text/javascript">
	// videoplayer hotkeys

	// get videojs player
	var player = videojs('my_video');

	// should we run a hotkey event (or is it a conflicting event that we need to avoid)
	// e: activeElement
	// key : event.key
	function handleHotkeys(key) {
		e = document.activeElement;
		// not a keypress ? somethig is messed up
		if (!key) {
			return false;
		}
		// nothing focused
		if (!e) {
			return true;
		}
		// global player or page focus
		if (e.tagName.toLowerCase()=="body" || e.tagName.toLowerCase()=="video") {
			return true;
		}
		if (e.classList.contains("vjs-play-control") && (key=="ArrowLeft" || key=="ArrowRight")) {
			return true;
		}

		return false;
	}

	document.addEventListener('keyup', event => {
		console.log("keypress event for hotkeys");
		// prevent custom hotkey usage if we have some conflicting or unknown focus (e.g. if we have the "play" button focused, dont double-run the same event with space key)
		if (!handleHotkeys(event.key)) {
			return;
		}
		// handle custom hotkey
		switch (event.key) {
		case " ":
			if (player.paused()) {
				player.play();
				console.log("play");
			} else {
				player.pause();
				console.log("pause");
			}
			break;
		case "ArrowLeft":
			player.currentTime(player.currentTime() - 5);
			console.log("-5");
			break;
		case "ArrowRight":
			player.currentTime(player.currentTime() + 5);
			console.log("+5");
			break;
		}
	});

	function removeFocus() {
		if (document.activeElement && document.fullscreenElement) {
			document.activeElement.blur();
		}
	}
	/* Standard syntax */
	document.addEventListener("fullscreenchange", function() {
		removeFocus();
	});

	/* Firefox */
	document.addEventListener("mozfullscreenchange", function() {
		removeFocus();
	});

	/* Chrome, Safari and Opera */
	document.addEventListener("webkitfullscreenchange", function() {
		removeFocus();
	});

	/* IE / Edge */
	document.addEventListener("msfullscreenchange", function() {
		removeFocus();
	});
	</script>
	<?php if($login_user != null && $login_user == $video_object->user){?>
        <script type="text/javascript">
                // parse timecode formatr h:min:sec to sec
                function hour_to_sec(hour, min, sec){
                    var hour_sec = hour * 3600;
                    var min_sec = min * 60;
                    return hour_sec + min_sec + Number(sec);
                }
                // parse timecode formatr sec to h:min:sec 
                function sec_to_hour(sec){
                    var hour_full = sec / 3600;
                    var time_hour = Math.trunc(hour_full);
                    var min_full = (hour_full - time_hour) * 60;
                    var time_min = Math.trunc(min_full);
                    var time_sec = (min_full - time_min) * 60;
                    return {
                        hour : time_hour,
                        min : time_min,
                        sec : time_sec.toFixed(2)
                    };
                }
                function display_time(time_sec){
                    var time_hour = sec_to_hour(time_sec);
                    var time_sting = null;
                    if(time_hour['hour'] !== 0){
                        time_sting = time_hour['hour'] + ' h ' + time_hour['min'] + ' min ' + time_hour['sec'] + ' sec';
                    }else if(time_hour['min'] !== 0){
                        time_sting = time_hour['min'] + ' min ' + time_hour['sec'] + ' sec';
                    }else{
                        time_sting = time_hour['sec'] + ' sec';
                    }
                    return time_sting;
                }
                // get duration video
                function set_edit_duration(){
                    var full_duration = document.getElementById('my_video_html5_api').duration;
                    var offset_end_player = player._offsetEnd;
                    var offset_start_player = player._offsetStart;
                    if(edit_info['start'] === null && edit_info['end'] === null){
                        if(offset_start_player === 0 && offset_end_player === 0){
                            edit_info['duration'] = full_duration;
                        }else if(offset_start_player !== 0 && offset_end_player === 0){
                            edit_info['duration'] = full_duration - offset_start_player;
                        }else if(offset_start_player === 0 && offset_end_player !== 0){
                            edit_info['duration'] = offset_end_player;
                        }else{
                            edit_info['duration'] = offset_end_player - offset_start_player;
                        }
                    }else if(edit_info['start'] !== null && edit_info['end'] === null){
                        edit_info['duration'] = full_duration - (edit_info['start'] + offset_start_player);
                    }else if(edit_info['start'] === null && edit_info['end'] !== null){
                        edit_info['duration'] = edit_info['end'];
                    }else{
                        edit_info['duration'] = edit_info['end'] - (edit_info['start'] + offset_start_player);
                    }
                }
                
                function set_edit_recap(){
                    var span_start = document.getElementById('get_start');
                    var span_end = document.getElementById('get_end');
                    var span_tc = document.getElementById('get_tc');
                    if(edit_info['start'] !== null){
                        span_start.innerText = display_time(edit_info['start']);
                    }
                    if(edit_info['end'] !== null){
                        span_end.innerText = display_time(edit_info['end']);
                    }
                    if(edit_info['tc'] !== null){
                        span_tc.innerText = display_time(edit_info['tc']);
                    }
                }
                
                function set_edit(param_set) {
                    var error = false;
                    var error_message = '';
                    var error_edit = document.getElementById('error_edit');
                    set_edit_duration();
                    var tmp_timecode = player.currentTime();
                    if(param_set === 'start'){
                        if(edit_info['end'] !== null){
                            if(edit_info['end'] <= tmp_timecode){
                                error = true;
                                error_message = 'Vérifiez que le début indiqué n\'est pas supérieure à la fin indiquée';
                            }else{
                                edit_info[param_set] = tmp_timecode;
                            }
                        }else{
                            edit_info[param_set] = tmp_timecode;
                        }
                    }
                    if(param_set === 'end'){
                        if(edit_info['start'] !== null){
                            if((tmp_timecode === 0 || tmp_timecode <= edit_info['start'])){
                                error = true;
                                error_message = 'Vérifiez que la fin indiqué n\'est pas infèrieure au début indiqué, ni égale à zéro';
                            }else{
                                edit_info[param_set] = tmp_timecode;
                            }
                        }else{
                            edit_info[param_set] = tmp_timecode;
                        }
                    }
                    if(param_set === 'tc'){
                        if(edit_info['start'] !== null && edit_info['end'] !== null){
                            if(tmp_timecode < edit_info['start'] && tmp_timecode > edit_info['end']){
                                error = null;
                                error_message = 'Vérifiez que le momment indiqué est bien compris entre le début est la fin indiquée';
                            }else{
                                edit_info[param_set] = tmp_timecode;
                            }
                        }else{
                           edit_info[param_set] = tmp_timecode; 
                        }
                    }
                    if(!error){
                        edit_link.href = window.location.href.split('&')[0];
                        if(edit_info['start'] !== null && edit_info['start'] !== 0){
                            edit_link.href += '&start=' + edit_info['start'];
                        }
                        if(edit_info['end'] !== null && edit_info['end'] !== 0){
                            edit_link.href += '&end=' + edit_info['end'];
                        }
                        if(edit_info['tc'] !== null && edit_info['tc'] !== 0){
                            edit_link.href += '&tc=' + edit_info['tc'];
                        }
                        edit_link.innerText = edit_link.href;
                        error_edit.innerText = '';
                        set_edit_recap();
                    }else{
                        error_edit.innerText = error_message;
                        edit_link.href = '';
                        edit_link.innerText = '';
                    }
                }
                // get offesets and timecode
                var tc_element = document.getElementById('tc');
                var offset_start_element = document.getElementById('offset_start');
                var offset_end_element = document.getElementById('offset_end');
                var tc = Number(tc_element.innerHTML);
                var offset_start = Number(offset_start_element.innerHTML);
                var offset_end = Number(offset_end_element.innerHTML);
                var btn_set_start = document.getElementById('set_start');
                var btn_set_end = document.getElementById('set_end');
                var btn_set_tc = document.getElementById('set_tc');
                var edit_link = document.getElementById('edit_link');
                var edit_info = {
                    duration : null,
                    start : null,
                    end : null,
                    tc : null
                };
                edit_link.href = window.location;
                edit_link.innerText = window.location;
                
                // when player ready set offset and timecode
                player.ready(function(){
                    //player.play();
                    player.offset({
                       start : offset_start,
                       end : offset_end,
                       restart_beginning: true
                    });
                    player.currentTime(tc-offset_start);
                });

                
                btn_set_start.addEventListener('click', function(){set_edit('start');});
                btn_set_end.addEventListener('click', function(){set_edit('end');});
                btn_set_tc.addEventListener('click', function(){set_edit('tc');});
        </script>
	<?php } ?>
    </body>
</html>
