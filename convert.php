<?php
// put that in crontab

chdir(__DIR__);


$AAC_ENCODER_LIB="aac";
// $AAC_ENCODER_LIB=libfdk_aac";

if(file_exists('inc/config.php')) {
	// can override AAC_ENCODER_LIB
	include("inc/config.php");
}


define("TEMP_DIR","/tmp");


$totalstarttime=time();

function clierror($msg) {
	echo(date(DATE_ATOM).": ".$msg);
	return false;
}

// fail with false if blocking
function lock($lockstr) {
	$lockfile=TEMP_DIR.'/course-record-flock'.$lockstr;
	$fp = fopen($lockfile, 'a+');
	if(!flock($fp, LOCK_EX | LOCK_NB)) {
		fclose($fp);
		return null;
	}
	return $fp;
}
function unlock($fp) {
	flock($fp, LOCK_UN);
	fclose($fp);
}
function getload() {
	$procloadavg=file_get_contents("/proc/loadavg");
	if (!$procloadavg) {
		return clierror("can't check /proc/loadavg, can't read file");
	}
	$load1=explode(" ", $procloadavg)[0];
	if (!is_numeric($load1)) {
		return clierror("can't check /proc/loadavg, not numeric: ".$load1);
	}
	return ((float) $load1);
}

function getfreemem() {
	$fh = fopen('/proc/meminfo','r');
	if ($fh===false) {
		return clierror("can't check /proc/meminfo, can't read file");
	}
	$mem = -1;
	while ($line = fgets($fh)) {
		$pieces = array();
		if (preg_match('/^MemAvailable:\s+(\d+)\skB$/', $line, $pieces)) {
			$memavailable = $pieces[1];
			continue;
		}
		if (preg_match('/^MemTotal:\s+(\d+)\skB$/', $line, $pieces)) {
			$memtotal = $pieces[1];
			continue;
		}
	}
	if ($memavailable<0 || !is_numeric($memavailable)) {
		return clierror("can't check /proc/meminfo, MemAvailable entry not found");
	}
	if ($memtotal<0 || !is_numeric($memtotal)) {
		return clierror("can't check /proc/meminfo, MemTotal entry not found");
	}
	fclose($fh);
	return (int) ($memavailable*100/$memtotal);
}

// return true on success
function runffmpeg($cmd, $hlog) {
	fwrite($hlog, date(DATE_ATOM)." === Running command: ".$cmd."\n");
	$data="";
	$starttime=time();
	$proc=popen($cmd, "r");
	while (!feof($proc)) {
		$block=fread($proc, 4096);
		$data.=$block;
		fwrite($hlog, $block);
	}
	$status=pclose($proc);
	if ($status===0) {
		$statusstr="sucessfull";
	} else {
		$statusstr="failed";
	}
	$elapsedtime=time()-$starttime;
	fwrite($hlog, date(DATE_ATOM)."=== Run $statusstr, elapsed ${elapsedtime}s, status: $status");
	if ($status===0) {
		return($data);
	}
	return null;
}
function fileloop() {
	$files = glob('uploads/*.*');
	usort($files, function($a, $b) {
		return filemtime($a) < filemtime($b);
	});
	$GLOBALS['countsuccess']=$GLOBALS['countmax']=0;
	$skip=0;
	foreach ($files as $f) {
		$load=getload();
		if ($load===false) {
			break;
		}
		if ($load>4) {
			clierror("load too high: ".$load."\n");
			break;
		}
		$mem=getfreemem();
		if ($mem===false) {
			break;
		}
		if ($mem<50) {
			clierror("mem too low: ".$mem."% of memory is available\n");
			break;
		}
		if (filemtime($f)+30>time()) {
			// file still beeing written
			echo("skip $f, upload in progress\n");
			continue;
		}
		$fp=pathinfo($f);
		$feco='v/'.$fp['filename'].'-eco.mp4';
		$ffail='fail/'.$fp['filename'].'-eco.mp4';
		$flog='logs/'.$fp['filename'].'.log';

		if (file_exists($feco) || file_exists($flog)) {
			// file already encoded or failed, skip
			$skip++;
			continue;
		}
		if ($skip>0) {
			echo("skipped $skip files, already encoded\n");
			$skip=0;
		}
		echo date(DATE_ATOM).": === encoding $f\n";
		$GLOBALS['countmax']++;
		$paramaf="";
		$paramvolgain="";
		$hlog=fopen($flog, "w");
		$cmd="/usr/bin/ffmpeg -i $f -async 1 -vn -af volumedetect -f null /dev/null 2>&1";
		$audiologs=runffmpeg($cmd,$hlog);
		if ($audiologs) {
			preg_match("/Parsed_volumedetect.* max_volume: -?(.*) dB/", $audiologs, $matches);
			if ($matches && count($matches)>1) {
				$volgain=$matches[1];
				$volgain--;
				if ($volgain>0 && $volgain<32) {
					fwrite($hlog, date(DATE_ATOM)."Volume adjusted : -${volgain}dB\n");
					$paramvolgain='volume='.$volgain.'dB';
					$paramaf='-filter:a "'.$paramvolgain.'"';
				}
			}
		}
#		$paramaf='-filter:a "'.$paramvolgain.'lv2=plugin=https\\\\\\://github.com/lucianodato/speech-denoiser"';

		// padding may be necessary because h264 codec reject size not divisble by 2
		$cmd="/usr/bin/ffmpeg -i $f -async 1 -max_muxing_queue_size 9999 -c:a ".$GLOBALS['AAC_ENCODER_LIB']." $paramaf -filter:v 'pad=ceil(iw/2)*2:ceil(ih/2)*2' -c:v libx264 -preset veryslow -threads 0 -movflags +faststart -pix_fmt yuv420p -maxrate 512k -bufsize 2048k -f mp4 -r 10 -vsync cfr -g 1800 -b:a 28k -crf 29 -vprofile high -level 4 $feco 2>&1";
		$result=runffmpeg($cmd, $hlog);
		if ($result) {
			$GLOBALS['countsuccess']++;
		} else {
			echo date(DATE_ATOM).": ERROR: failed to encode $feco, see $flog for logs and $ffail for broken file\n";
			if (file_exists($feco)) {
				rename($feco, $ffail);
			}
		}
		fclose($hlog);
		sleep(45);
	}
	if ($skip>0) {
		echo("skipped $skip files, already encoded\n");
		$skip=0;
	}
}
$lockfp=lock('encode');
if($lockfp===null) {
	// echo "already running\n";
	die();
}
fileloop();
unlock($lockfp);
$totalelapsedtime=time()-$totalstarttime;
echo date(DATE_ATOM).": === Encode task ran for ". $GLOBALS['countmax']." videos, success: ".$GLOBALS['countsuccess'].", failed: ".($GLOBALS['countmax']-$GLOBALS['countsuccess']).", elapsed time: ${totalelapsedtime}s\n";

?>
