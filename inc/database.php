<?php
require_once('config.php');
require_once('utility.php');


function dbescapehtml($str, $flags="") {
	return htmlspecialchars($str);
}

class Db {
	function __construct() {
		try {
			$this->dbh = new PDO("mysql:host=localhost;dbname=".DB_NAME.";charset=utf8mb4", DB_LOGIN, DB_PASSWORD);
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->dbh->setAttribute(PDO::ATTR_AUTOCOMMIT,0);
		} catch (PDOException $e) {
			$this->dbh=null;
			showerror($e->getMessage());
			return;
		}
		try {
			// $this->listconf_stmt=$this->dbh->prepare("SELECT id, confname, createtime FROM conf WHERE username=:username");
			// $this->deleteconf_stmt=$this->dbh->prepare("DELETE FROM conf WHERE id=:id AND username=:username");
			$this->insertvid_stmt=$this->dbh->prepare("INSERT INTO videos (filename, user) VALUES (:filename, :user)");
                        $this->getvid_stmt = $this->dbh->prepare("SELECT user, filename, deleted, title FROM videos WHERE filename LIKE :filename");
			$this->renamevid_stmt = $this->dbh->prepare("UPDATE videos SET title = :title WHERE id = :id AND user = :user");
			$this->deletevid_stmt = $this->dbh->prepare("UPDATE videos SET deleted = NOW() WHERE id = :id AND user = :user");
			$this->restorevid_stmt = $this->dbh->prepare("UPDATE videos SET deleted = NULL WHERE id = :id AND user = :user");
			$this->getvidsbyuser_stmt= $this->dbh->prepare("SELECT * FROM videos WHERE user = :user AND (deleted IS NULL OR deleted>DATE_SUB(NOW(), INTERVAL 14 DAY))");
			$this->getold_stmt= $this->dbh->prepare("SELECT filename FROM videos WHERE deleted IS NOT NULL AND deleted<NOW()-INTERVAL 15 DAY");
			// $this->confexists_stmt=$this->dbh->prepare("SELECT count(*) FROM conf WHERE confname=:confname AND username=:username");

			//$this->logguest_stmt=$this->dbh->prepare("INSERT INTO lastlogin (confid, guestname) SELECT conf.id, :guestname FROM conf WHERE username=:username AND confname=:confname");
		} catch (PDOException $e) {
			$this->dbh=null;
			showerror($e->getMessage());
			return;
		}
	}
	/*
	 * return the conf id
	 */
	function insertvid($filename, $user) {
		try {
			$stmtvalues = array(
				':filename'=>$filename,
				':user'=>$user
			);
			$this->dbh->beginTransaction();
			// strangly, for PDO lastInsertId is not reliable on mysql...
			$this->insertvid_stmt->execute($stmtvalues);
			$this->dbh->commit();
			return true;
		} catch (PDOException $e) {
			showerror("Impossible d'enregistrer la vidéo dans la base de donnée.<br>".$e->getMessage());
			return null;
		}
	}
        
        /*
         * return video object
         */
        function getvid($filename){
		$stmtvalues = array(
			':filename'=>$filename
		);
            try{
                $this->getvid_stmt->execute($stmtvalues);
                return $this->getvid_stmt->fetch(PDO::FETCH_OBJ);
                
            } catch (Exception $e) {
                showerror("Impossible de récupèrer la vidéo dans la base de donnée.<br>".$e->getMessage());
		return null;
            }
        }

	function getold() {
		try {
			$this->getold_stmt->execute();
			$results = $this->getold_stmt->fetchAll(PDO::FETCH_ASSOC);
			return $results;
		} catch (PDOException $e) {
			showerror("Impossible de récupérer la liste des vidéos a purger".$e->getMessage());
			return null;
		}
	}

	function getvidsbyuser($user) {
		$stmtvalues = array(
			':user'=>$user,
		);
		try {
			$this->getvidsbyuser_stmt->execute($stmtvalues);
			$results = $this->getvidsbyuser_stmt->fetchAll(PDO::FETCH_ASSOC);
			return $results;
		} catch (PDOException $e) {
			showerror("Impossible de récupérer la liste des vidéos.<br>".$e->getMessage());
			return null;
		}
	}

	function deletevid($id, $user) {
		$date = date("Ymd");
		$stmtvalues = array(
			':id'=>$id,
			':user'=>$user
		);
		try {
			$this->dbh->beginTransaction();
			// strangly, for PDO lastInsertId is not reliable on mysql...
			$this->deletevid_stmt->execute($stmtvalues);
			$this->dbh->commit();
			if ($this->deletevid_stmt->rowCount()!=1)
				return false;
			return true;
		} catch (PDOException $e) {
			showerror("Impossible de supprimer la vidéo.<br>".$e->getMessage());
			return false;
		}
	}

	function restorevid($id, $user) {
		$stmtvalues = array(
			':id'=>$id,
			':user'=>$user	
		);
		try {
			$this->dbh->beginTransaction();
			// strangly, for PDO lastInsertId is not reliable on mysql...
			$this->restorevid_stmt->execute($stmtvalues);
			$this->dbh->commit();
			if ($this->restorevid_stmt->rowCount()!=1)
				return false;
			return true;
		} catch (PDOException $e) {
			showerror("Impossible de restaurer la vidéo.<br>".$e->getMessage());
			return false;
		}
	}
	
	function renamevid($id, $title, $user) {
		$stmtvalues = array(
			':id'=>$id,
			':title'=>$title,
			':user'=>$user
		);
		try {
			$this->dbh->beginTransaction();
			// strangly, for PDO lastInsertId is not reliable on mysql...
			$this->renamevid_stmt->execute($stmtvalues);
			$this->dbh->commit();
			if ($this->renamevid_stmt->rowCount()!=1)
				return false;
			return true;
		} catch (PDOException $e) {
			showerror("Impossible de renommer la vidéo.<br>".$e->getMessage());
			return false;
		}
	}
}
?>
