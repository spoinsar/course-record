<?php

// a simple message display system
// 2 kinds of messages : error and success
//   - error stop everything and display itself
//   - success display a sig 
//
// buffering system : if the message comes too early, we buffer it until the headers are sent 
function showerror($e) {
	showmessage($e, 'errormsg');
}
function showmessage($m, $msgtype) {
	echo "<div class=\"$msgtype\">$m</div>";
}


?>
