<?php
require_once('inc/database.php');
$errorset=false;

if (!isset($_POST['user']) || !isset($_POST['mail']) || !isset($_POST['hash'])) {
	http_response_code(401);
	echo "Problème d'authentification : variables utilisateur non définies";
	exit;
}
$user=$_POST['user'];
$email_address=$_POST['mail'];
$hash=$_POST['hash'];
if (sha1(SECRET_UPLOAD_HASH.$user)!=$hash) {
	http_response_code(401);
	echo "Problème d'authentification : impossible de vérifier le hash";
	exit;
}

if (!isset($_POST['id']) || !isset($_POST['filename'])) {
	http_response_code(400);
	echo "Erreur de paramètres";
	exit;
}
$id=$_POST['id'];
$filename = $_POST['filename'];

$db = new Db;
if (!$db->restorevid($id, $user)) {
	http_response_code(409);
	echo "Impossible de restaurer la vidéo de la base de données";
	$errorset=true;
}


$path="deleted/$filename";
$ecofilename=pathinfo($filename)['filename']."-eco.mp4";
$ecopath="deleted/$ecofilename";

$worked=false;
if (file_exists($path)) {
	if (!rename($path, 'uploads/'.$filename) && !$errorset) {
		http_response_code(409);
		echo "Impossible de restaurer le fichier";
		$errorset=true;
	} else {
		if (!$errorset) {
			$worked=true;
		}
	}
}
if (file_exists($ecopath)) {
	if (!rename($ecopath, 'v/'.$ecofilename) && !$errorset) {
		http_response_code(409);
		echo "Impossible de restaurer le fichier";
		$errorset=true;
	} else {
		if (!$errorset) {
			$worked=true;
		}
	}
}
if (!$worked && !$errorset) {
	http_response_code(409);
	echo "Impossible de trouver un fichier vidéo à restaurer. La vidéo a t'elle déjà été restaurée précédement ?";
}
?>
